export type AmplifyDependentResourcesAttributes = {
    "auth": {
        "mentorappswiftc75237dc": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string",
            "UserPoolId": "string",
            "UserPoolArn": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string"
        }
    },
    "api": {
        "MentorAppSwift": {
            "GraphQLAPIKeyOutput": "string",
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    },
    "analytics": {
        "mentorappswift": {
            "Region": "string",
            "Id": "string",
            "appName": "string"
        }
    }
}