/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, annotate_overrides, dead_code, dead_codepublic_member_api_docs, depend_on_referenced_packages, file_names, library_private_types_in_public_api, no_leading_underscores_for_library_prefixes, no_leading_underscores_for_local_identifiers, non_constant_identifier_names, null_check_on_nullable_type_parameter, prefer_adjacent_string_concatenation, prefer_const_constructors, prefer_if_null_operators, prefer_interpolation_to_compose_strings, slash_for_doc_comments, sort_child_properties_last, unnecessary_const, unnecessary_constructor_name, unnecessary_late, unnecessary_new, unnecessary_null_aware_assignments, unnecessary_nullable_for_final_variable_declarations, unnecessary_string_interpolations, use_build_context_synchronously

import 'ModelProvider.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the Mentoria type in your schema. */
@immutable
class Mentoria extends Model {
  static const classType = const _MentoriaModelType();
  final String id;
  final String? _nombre;
  final String? _descripcion;
  final String? _topic;
  final TemporalDateTime? _fechaInicio;
  final TemporalDateTime? _fechaFin;
  final String? _ciudad;
  final List<UsuarioMentoria>? _usuarios;
  final String? _lugar;
  final List<MentoriaCategoria>? _Categorias;
  final int? _capacidad;
  final double? _precio;
  final bool? _esVirtual;
  final String? _usuarioID;
  final TemporalDateTime? _createdAt;
  final TemporalDateTime? _updatedAt;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  String? get nombre {
    return _nombre;
  }
  
  String? get descripcion {
    return _descripcion;
  }
  
  String? get topic {
    return _topic;
  }
  
  TemporalDateTime? get fechaInicio {
    return _fechaInicio;
  }
  
  TemporalDateTime? get fechaFin {
    return _fechaFin;
  }
  
  String? get ciudad {
    return _ciudad;
  }
  
  List<UsuarioMentoria>? get usuarios {
    return _usuarios;
  }
  
  String? get lugar {
    return _lugar;
  }
  
  List<MentoriaCategoria>? get Categorias {
    return _Categorias;
  }
  
  int? get capacidad {
    return _capacidad;
  }
  
  double? get precio {
    return _precio;
  }
  
  bool? get esVirtual {
    return _esVirtual;
  }
  
  String get usuarioID {
    try {
      return _usuarioID!;
    } catch(e) {
      throw new AmplifyCodeGenModelException(
          AmplifyExceptionMessages.codeGenRequiredFieldForceCastExceptionMessage,
          recoverySuggestion:
            AmplifyExceptionMessages.codeGenRequiredFieldForceCastRecoverySuggestion,
          underlyingException: e.toString()
          );
    }
  }
  
  TemporalDateTime? get createdAt {
    return _createdAt;
  }
  
  TemporalDateTime? get updatedAt {
    return _updatedAt;
  }
  
  const Mentoria._internal({required this.id, nombre, descripcion, topic, fechaInicio, fechaFin, ciudad, usuarios, lugar, Categorias, capacidad, precio, esVirtual, required usuarioID, createdAt, updatedAt}): _nombre = nombre, _descripcion = descripcion, _topic = topic, _fechaInicio = fechaInicio, _fechaFin = fechaFin, _ciudad = ciudad, _usuarios = usuarios, _lugar = lugar, _Categorias = Categorias, _capacidad = capacidad, _precio = precio, _esVirtual = esVirtual, _usuarioID = usuarioID, _createdAt = createdAt, _updatedAt = updatedAt;
  
  factory Mentoria({String? id, String? nombre, String? descripcion, String? topic, TemporalDateTime? fechaInicio, TemporalDateTime? fechaFin, String? ciudad, List<UsuarioMentoria>? usuarios, String? lugar, List<MentoriaCategoria>? Categorias, int? capacidad, double? precio, bool? esVirtual, required String usuarioID}) {
    return Mentoria._internal(
      id: id == null ? UUID.getUUID() : id,
      nombre: nombre,
      descripcion: descripcion,
      topic: topic,
      fechaInicio: fechaInicio,
      fechaFin: fechaFin,
      ciudad: ciudad,
      usuarios: usuarios != null ? List<UsuarioMentoria>.unmodifiable(usuarios) : usuarios,
      lugar: lugar,
      Categorias: Categorias != null ? List<MentoriaCategoria>.unmodifiable(Categorias) : Categorias,
      capacidad: capacidad,
      precio: precio,
      esVirtual: esVirtual,
      usuarioID: usuarioID);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Mentoria &&
      id == other.id &&
      _nombre == other._nombre &&
      _descripcion == other._descripcion &&
      _topic == other._topic &&
      _fechaInicio == other._fechaInicio &&
      _fechaFin == other._fechaFin &&
      _ciudad == other._ciudad &&
      DeepCollectionEquality().equals(_usuarios, other._usuarios) &&
      _lugar == other._lugar &&
      DeepCollectionEquality().equals(_Categorias, other._Categorias) &&
      _capacidad == other._capacidad &&
      _precio == other._precio &&
      _esVirtual == other._esVirtual &&
      _usuarioID == other._usuarioID;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("Mentoria {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("nombre=" + "$_nombre" + ", ");
    buffer.write("descripcion=" + "$_descripcion" + ", ");
    buffer.write("topic=" + "$_topic" + ", ");
    buffer.write("fechaInicio=" + (_fechaInicio != null ? _fechaInicio!.format() : "null") + ", ");
    buffer.write("fechaFin=" + (_fechaFin != null ? _fechaFin!.format() : "null") + ", ");
    buffer.write("ciudad=" + "$_ciudad" + ", ");
    buffer.write("lugar=" + "$_lugar" + ", ");
    buffer.write("capacidad=" + (_capacidad != null ? _capacidad!.toString() : "null") + ", ");
    buffer.write("precio=" + (_precio != null ? _precio!.toString() : "null") + ", ");
    buffer.write("esVirtual=" + (_esVirtual != null ? _esVirtual!.toString() : "null") + ", ");
    buffer.write("usuarioID=" + "$_usuarioID" + ", ");
    buffer.write("createdAt=" + (_createdAt != null ? _createdAt!.format() : "null") + ", ");
    buffer.write("updatedAt=" + (_updatedAt != null ? _updatedAt!.format() : "null"));
    buffer.write("}");
    
    return buffer.toString();
  }
  
  Mentoria copyWith({String? id, String? nombre, String? descripcion, String? topic, TemporalDateTime? fechaInicio, TemporalDateTime? fechaFin, String? ciudad, List<UsuarioMentoria>? usuarios, String? lugar, List<MentoriaCategoria>? Categorias, int? capacidad, double? precio, bool? esVirtual, String? usuarioID}) {
    return Mentoria._internal(
      id: id ?? this.id,
      nombre: nombre ?? this.nombre,
      descripcion: descripcion ?? this.descripcion,
      topic: topic ?? this.topic,
      fechaInicio: fechaInicio ?? this.fechaInicio,
      fechaFin: fechaFin ?? this.fechaFin,
      ciudad: ciudad ?? this.ciudad,
      usuarios: usuarios ?? this.usuarios,
      lugar: lugar ?? this.lugar,
      Categorias: Categorias ?? this.Categorias,
      capacidad: capacidad ?? this.capacidad,
      precio: precio ?? this.precio,
      esVirtual: esVirtual ?? this.esVirtual,
      usuarioID: usuarioID ?? this.usuarioID);
  }
  
  Mentoria.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _nombre = json['nombre'],
      _descripcion = json['descripcion'],
      _topic = json['topic'],
      _fechaInicio = json['fechaInicio'] != null ? TemporalDateTime.fromString(json['fechaInicio']) : null,
      _fechaFin = json['fechaFin'] != null ? TemporalDateTime.fromString(json['fechaFin']) : null,
      _ciudad = json['ciudad'],
      _usuarios = json['usuarios'] is List
        ? (json['usuarios'] as List)
          .where((e) => e?['serializedData'] != null)
          .map((e) => UsuarioMentoria.fromJson(new Map<String, dynamic>.from(e['serializedData'])))
          .toList()
        : null,
      _lugar = json['lugar'],
      _Categorias = json['Categorias'] is List
        ? (json['Categorias'] as List)
          .where((e) => e?['serializedData'] != null)
          .map((e) => MentoriaCategoria.fromJson(new Map<String, dynamic>.from(e['serializedData'])))
          .toList()
        : null,
      _capacidad = (json['capacidad'] as num?)?.toInt(),
      _precio = (json['precio'] as num?)?.toDouble(),
      _esVirtual = json['esVirtual'],
      _usuarioID = json['usuarioID'],
      _createdAt = json['createdAt'] != null ? TemporalDateTime.fromString(json['createdAt']) : null,
      _updatedAt = json['updatedAt'] != null ? TemporalDateTime.fromString(json['updatedAt']) : null;
  
  Map<String, dynamic> toJson() => {
    'id': id, 'nombre': _nombre, 'descripcion': _descripcion, 'topic': _topic, 'fechaInicio': _fechaInicio?.format(), 'fechaFin': _fechaFin?.format(), 'ciudad': _ciudad, 'usuarios': _usuarios?.map((UsuarioMentoria? e) => e?.toJson()).toList(), 'lugar': _lugar, 'Categorias': _Categorias?.map((MentoriaCategoria? e) => e?.toJson()).toList(), 'capacidad': _capacidad, 'precio': _precio, 'esVirtual': _esVirtual, 'usuarioID': _usuarioID, 'createdAt': _createdAt?.format(), 'updatedAt': _updatedAt?.format()
  };
  
  Map<String, Object?> toMap() => {
    'id': id, 'nombre': _nombre, 'descripcion': _descripcion, 'topic': _topic, 'fechaInicio': _fechaInicio, 'fechaFin': _fechaFin, 'ciudad': _ciudad, 'usuarios': _usuarios, 'lugar': _lugar, 'Categorias': _Categorias, 'capacidad': _capacidad, 'precio': _precio, 'esVirtual': _esVirtual, 'usuarioID': _usuarioID, 'createdAt': _createdAt, 'updatedAt': _updatedAt
  };

  static final QueryField ID = QueryField(fieldName: "id");
  static final QueryField NOMBRE = QueryField(fieldName: "nombre");
  static final QueryField DESCRIPCION = QueryField(fieldName: "descripcion");
  static final QueryField TOPIC = QueryField(fieldName: "topic");
  static final QueryField FECHAINICIO = QueryField(fieldName: "fechaInicio");
  static final QueryField FECHAFIN = QueryField(fieldName: "fechaFin");
  static final QueryField CIUDAD = QueryField(fieldName: "ciudad");
  static final QueryField USUARIOS = QueryField(
    fieldName: "usuarios",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (UsuarioMentoria).toString()));
  static final QueryField LUGAR = QueryField(fieldName: "lugar");
  static final QueryField CATEGORIAS = QueryField(
    fieldName: "Categorias",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (MentoriaCategoria).toString()));
  static final QueryField CAPACIDAD = QueryField(fieldName: "capacidad");
  static final QueryField PRECIO = QueryField(fieldName: "precio");
  static final QueryField ESVIRTUAL = QueryField(fieldName: "esVirtual");
  static final QueryField USUARIOID = QueryField(fieldName: "usuarioID");
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Mentoria";
    modelSchemaDefinition.pluralName = "Mentorias";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PUBLIC,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.indexes = [
      ModelIndex(fields: const ["usuarioID"], name: "byUsuario")
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Mentoria.NOMBRE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Mentoria.DESCRIPCION,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Mentoria.TOPIC,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Mentoria.FECHAINICIO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Mentoria.FECHAFIN,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Mentoria.CIUDAD,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.hasMany(
      key: Mentoria.USUARIOS,
      isRequired: false,
      ofModelName: (UsuarioMentoria).toString(),
      associatedKey: UsuarioMentoria.MENTORIA
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Mentoria.LUGAR,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.hasMany(
      key: Mentoria.CATEGORIAS,
      isRequired: false,
      ofModelName: (MentoriaCategoria).toString(),
      associatedKey: MentoriaCategoria.MENTORIA
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Mentoria.CAPACIDAD,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.int)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Mentoria.PRECIO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.double)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Mentoria.ESVIRTUAL,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.bool)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Mentoria.USUARIOID,
      isRequired: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'createdAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'updatedAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
  });
}

class _MentoriaModelType extends ModelType<Mentoria> {
  const _MentoriaModelType();
  
  @override
  Mentoria fromJson(Map<String, dynamic> jsonData) {
    return Mentoria.fromJson(jsonData);
  }
}