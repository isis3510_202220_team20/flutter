/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, annotate_overrides, dead_code, dead_codepublic_member_api_docs, depend_on_referenced_packages, file_names, library_private_types_in_public_api, no_leading_underscores_for_library_prefixes, no_leading_underscores_for_local_identifiers, non_constant_identifier_names, null_check_on_nullable_type_parameter, prefer_adjacent_string_concatenation, prefer_const_constructors, prefer_if_null_operators, prefer_interpolation_to_compose_strings, slash_for_doc_comments, sort_child_properties_last, unnecessary_const, unnecessary_constructor_name, unnecessary_late, unnecessary_new, unnecessary_null_aware_assignments, unnecessary_nullable_for_final_variable_declarations, unnecessary_string_interpolations, use_build_context_synchronously

import 'ModelProvider.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the Categoria type in your schema. */
@immutable
class Categoria extends Model {
  static const classType = const _CategoriaModelType();
  final String id;
  final String? _nombre;
  final String? _valor;
  final List<MentoriaCategoria>? _mentorias;
  final TemporalDateTime? _createdAt;
  final TemporalDateTime? _updatedAt;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  String? get nombre {
    return _nombre;
  }
  
  String? get valor {
    return _valor;
  }
  
  List<MentoriaCategoria>? get mentorias {
    return _mentorias;
  }
  
  TemporalDateTime? get createdAt {
    return _createdAt;
  }
  
  TemporalDateTime? get updatedAt {
    return _updatedAt;
  }
  
  const Categoria._internal({required this.id, nombre, valor, mentorias, createdAt, updatedAt}): _nombre = nombre, _valor = valor, _mentorias = mentorias, _createdAt = createdAt, _updatedAt = updatedAt;
  
  factory Categoria({String? id, String? nombre, String? valor, List<MentoriaCategoria>? mentorias}) {
    return Categoria._internal(
      id: id == null ? UUID.getUUID() : id,
      nombre: nombre,
      valor: valor,
      mentorias: mentorias != null ? List<MentoriaCategoria>.unmodifiable(mentorias) : mentorias);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Categoria &&
      id == other.id &&
      _nombre == other._nombre &&
      _valor == other._valor &&
      DeepCollectionEquality().equals(_mentorias, other._mentorias);
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("Categoria {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("nombre=" + "$_nombre" + ", ");
    buffer.write("valor=" + "$_valor" + ", ");
    buffer.write("createdAt=" + (_createdAt != null ? _createdAt!.format() : "null") + ", ");
    buffer.write("updatedAt=" + (_updatedAt != null ? _updatedAt!.format() : "null"));
    buffer.write("}");
    
    return buffer.toString();
  }
  
  Categoria copyWith({String? id, String? nombre, String? valor, List<MentoriaCategoria>? mentorias}) {
    return Categoria._internal(
      id: id ?? this.id,
      nombre: nombre ?? this.nombre,
      valor: valor ?? this.valor,
      mentorias: mentorias ?? this.mentorias);
  }
  
  Categoria.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _nombre = json['nombre'],
      _valor = json['valor'],
      _mentorias = json['mentorias'] is List
        ? (json['mentorias'] as List)
          .where((e) => e?['serializedData'] != null)
          .map((e) => MentoriaCategoria.fromJson(new Map<String, dynamic>.from(e['serializedData'])))
          .toList()
        : null,
      _createdAt = json['createdAt'] != null ? TemporalDateTime.fromString(json['createdAt']) : null,
      _updatedAt = json['updatedAt'] != null ? TemporalDateTime.fromString(json['updatedAt']) : null;
  
  Map<String, dynamic> toJson() => {
    'id': id, 'nombre': _nombre, 'valor': _valor, 'mentorias': _mentorias?.map((MentoriaCategoria? e) => e?.toJson()).toList(), 'createdAt': _createdAt?.format(), 'updatedAt': _updatedAt?.format()
  };
  
  Map<String, Object?> toMap() => {
    'id': id, 'nombre': _nombre, 'valor': _valor, 'mentorias': _mentorias, 'createdAt': _createdAt, 'updatedAt': _updatedAt
  };

  static final QueryField ID = QueryField(fieldName: "id");
  static final QueryField NOMBRE = QueryField(fieldName: "nombre");
  static final QueryField VALOR = QueryField(fieldName: "valor");
  static final QueryField MENTORIAS = QueryField(
    fieldName: "mentorias",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (MentoriaCategoria).toString()));
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Categoria";
    modelSchemaDefinition.pluralName = "Categorias";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PUBLIC,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Categoria.NOMBRE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Categoria.VALOR,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.hasMany(
      key: Categoria.MENTORIAS,
      isRequired: false,
      ofModelName: (MentoriaCategoria).toString(),
      associatedKey: MentoriaCategoria.CATEGORIA
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'createdAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'updatedAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
  });
}

class _CategoriaModelType extends ModelType<Categoria> {
  const _CategoriaModelType();
  
  @override
  Categoria fromJson(Map<String, dynamic> jsonData) {
    return Categoria.fromJson(jsonData);
  }
}