/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, annotate_overrides, dead_code, dead_codepublic_member_api_docs, depend_on_referenced_packages, file_names, library_private_types_in_public_api, no_leading_underscores_for_library_prefixes, no_leading_underscores_for_local_identifiers, non_constant_identifier_names, null_check_on_nullable_type_parameter, prefer_adjacent_string_concatenation, prefer_const_constructors, prefer_if_null_operators, prefer_interpolation_to_compose_strings, slash_for_doc_comments, sort_child_properties_last, unnecessary_const, unnecessary_constructor_name, unnecessary_late, unnecessary_new, unnecessary_null_aware_assignments, unnecessary_nullable_for_final_variable_declarations, unnecessary_string_interpolations, use_build_context_synchronously

import 'ModelProvider.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the Usuario type in your schema. */
@immutable
class Usuario extends Model {
  static const classType = const _UsuarioModelType();
  final String id;
  final String? _nombre_apellido;
  final String? _email;
  final String? _biografia;
  final double? _calificacion;
  final List<UsuarioMentoria>? _EstudiantesMentoria;
  final bool? _esProfesor;
  final String? _username;
  final TemporalDateTime? _createdAt;
  final TemporalDateTime? _updatedAt;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  String? get nombre_apellido {
    return _nombre_apellido;
  }
  
  String? get email {
    return _email;
  }
  
  String? get biografia {
    return _biografia;
  }
  
  double? get calificacion {
    return _calificacion;
  }
  
  List<UsuarioMentoria>? get EstudiantesMentoria {
    return _EstudiantesMentoria;
  }
  
  bool? get esProfesor {
    return _esProfesor;
  }
  
  String? get username {
    return _username;
  }
  
  TemporalDateTime? get createdAt {
    return _createdAt;
  }
  
  TemporalDateTime? get updatedAt {
    return _updatedAt;
  }
  
  const Usuario._internal({required this.id, nombre_apellido, email, biografia, calificacion, EstudiantesMentoria, esProfesor, username, createdAt, updatedAt}): _nombre_apellido = nombre_apellido, _email = email, _biografia = biografia, _calificacion = calificacion, _EstudiantesMentoria = EstudiantesMentoria, _esProfesor = esProfesor, _username = username, _createdAt = createdAt, _updatedAt = updatedAt;
  
  factory Usuario({String? id, String? nombre_apellido, String? email, String? biografia, double? calificacion, List<UsuarioMentoria>? EstudiantesMentoria, bool? esProfesor, String? username}) {
    return Usuario._internal(
      id: id == null ? UUID.getUUID() : id,
      nombre_apellido: nombre_apellido,
      email: email,
      biografia: biografia,
      calificacion: calificacion,
      EstudiantesMentoria: EstudiantesMentoria != null ? List<UsuarioMentoria>.unmodifiable(EstudiantesMentoria) : EstudiantesMentoria,
      esProfesor: esProfesor,
      username: username);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Usuario &&
      id == other.id &&
      _nombre_apellido == other._nombre_apellido &&
      _email == other._email &&
      _biografia == other._biografia &&
      _calificacion == other._calificacion &&
      DeepCollectionEquality().equals(_EstudiantesMentoria, other._EstudiantesMentoria) &&
      _esProfesor == other._esProfesor &&
      _username == other._username;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("Usuario {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("nombre_apellido=" + "$_nombre_apellido" + ", ");
    buffer.write("email=" + "$_email" + ", ");
    buffer.write("biografia=" + "$_biografia" + ", ");
    buffer.write("calificacion=" + (_calificacion != null ? _calificacion!.toString() : "null") + ", ");
    buffer.write("esProfesor=" + (_esProfesor != null ? _esProfesor!.toString() : "null") + ", ");
    buffer.write("username=" + "$_username" + ", ");
    buffer.write("createdAt=" + (_createdAt != null ? _createdAt!.format() : "null") + ", ");
    buffer.write("updatedAt=" + (_updatedAt != null ? _updatedAt!.format() : "null"));
    buffer.write("}");
    
    return buffer.toString();
  }
  
  Usuario copyWith({String? id, String? nombre_apellido, String? email, String? biografia, double? calificacion, List<UsuarioMentoria>? EstudiantesMentoria, bool? esProfesor, String? username}) {
    return Usuario._internal(
      id: id ?? this.id,
      nombre_apellido: nombre_apellido ?? this.nombre_apellido,
      email: email ?? this.email,
      biografia: biografia ?? this.biografia,
      calificacion: calificacion ?? this.calificacion,
      EstudiantesMentoria: EstudiantesMentoria ?? this.EstudiantesMentoria,
      esProfesor: esProfesor ?? this.esProfesor,
      username: username ?? this.username);
  }
  
  Usuario.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _nombre_apellido = json['nombre_apellido'],
      _email = json['email'],
      _biografia = json['biografia'],
      _calificacion = (json['calificacion'] as num?)?.toDouble(),
      _EstudiantesMentoria = json['EstudiantesMentoria'] is List
        ? (json['EstudiantesMentoria'] as List)
          .where((e) => e?['serializedData'] != null)
          .map((e) => UsuarioMentoria.fromJson(new Map<String, dynamic>.from(e['serializedData'])))
          .toList()
        : null,
      _esProfesor = json['esProfesor'],
      _username = json['username'],
      _createdAt = json['createdAt'] != null ? TemporalDateTime.fromString(json['createdAt']) : null,
      _updatedAt = json['updatedAt'] != null ? TemporalDateTime.fromString(json['updatedAt']) : null;
  
  Map<String, dynamic> toJson() => {
    'id': id, 'nombre_apellido': _nombre_apellido, 'email': _email, 'biografia': _biografia, 'calificacion': _calificacion, 'EstudiantesMentoria': _EstudiantesMentoria?.map((UsuarioMentoria? e) => e?.toJson()).toList(), 'esProfesor': _esProfesor, 'username': _username, 'createdAt': _createdAt?.format(), 'updatedAt': _updatedAt?.format()
  };
  
  Map<String, Object?> toMap() => {
    'id': id, 'nombre_apellido': _nombre_apellido, 'email': _email, 'biografia': _biografia, 'calificacion': _calificacion, 'EstudiantesMentoria': _EstudiantesMentoria, 'esProfesor': _esProfesor, 'username': _username, 'createdAt': _createdAt, 'updatedAt': _updatedAt
  };

  static final QueryField ID = QueryField(fieldName: "id");
  static final QueryField NOMBRE_APELLIDO = QueryField(fieldName: "nombre_apellido");
  static final QueryField EMAIL = QueryField(fieldName: "email");
  static final QueryField BIOGRAFIA = QueryField(fieldName: "biografia");
  static final QueryField CALIFICACION = QueryField(fieldName: "calificacion");
  static final QueryField ESTUDIANTESMENTORIA = QueryField(
    fieldName: "EstudiantesMentoria",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (UsuarioMentoria).toString()));
  static final QueryField ESPROFESOR = QueryField(fieldName: "esProfesor");
  static final QueryField USERNAME = QueryField(fieldName: "username");
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Usuario";
    modelSchemaDefinition.pluralName = "Usuarios";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PUBLIC,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.NOMBRE_APELLIDO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.EMAIL,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.BIOGRAFIA,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.CALIFICACION,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.double)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.hasMany(
      key: Usuario.ESTUDIANTESMENTORIA,
      isRequired: false,
      ofModelName: (UsuarioMentoria).toString(),
      associatedKey: UsuarioMentoria.USUARIO
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.ESPROFESOR,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.bool)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.USERNAME,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'createdAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'updatedAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
  });
}

class _UsuarioModelType extends ModelType<Usuario> {
  const _UsuarioModelType();
  
  @override
  Usuario fromJson(Map<String, dynamic> jsonData) {
    return Usuario.fromJson(jsonData);
  }
}