/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, annotate_overrides, dead_code, dead_codepublic_member_api_docs, depend_on_referenced_packages, file_names, library_private_types_in_public_api, no_leading_underscores_for_library_prefixes, no_leading_underscores_for_local_identifiers, non_constant_identifier_names, null_check_on_nullable_type_parameter, prefer_adjacent_string_concatenation, prefer_const_constructors, prefer_if_null_operators, prefer_interpolation_to_compose_strings, slash_for_doc_comments, sort_child_properties_last, unnecessary_const, unnecessary_constructor_name, unnecessary_late, unnecessary_new, unnecessary_null_aware_assignments, unnecessary_nullable_for_final_variable_declarations, unnecessary_string_interpolations, use_build_context_synchronously

import 'ModelProvider.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the UsuarioMentoria type in your schema. */
@immutable
class UsuarioMentoria extends Model {
  static const classType = const _UsuarioMentoriaModelType();
  final String id;
  final Mentoria? _mentoria;
  final Usuario? _usuario;
  final TemporalDateTime? _createdAt;
  final TemporalDateTime? _updatedAt;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  Mentoria get mentoria {
    try {
      return _mentoria!;
    } catch(e) {
      throw new AmplifyCodeGenModelException(
          AmplifyExceptionMessages.codeGenRequiredFieldForceCastExceptionMessage,
          recoverySuggestion:
            AmplifyExceptionMessages.codeGenRequiredFieldForceCastRecoverySuggestion,
          underlyingException: e.toString()
          );
    }
  }
  
  Usuario get usuario {
    try {
      return _usuario!;
    } catch(e) {
      throw new AmplifyCodeGenModelException(
          AmplifyExceptionMessages.codeGenRequiredFieldForceCastExceptionMessage,
          recoverySuggestion:
            AmplifyExceptionMessages.codeGenRequiredFieldForceCastRecoverySuggestion,
          underlyingException: e.toString()
          );
    }
  }
  
  TemporalDateTime? get createdAt {
    return _createdAt;
  }
  
  TemporalDateTime? get updatedAt {
    return _updatedAt;
  }
  
  const UsuarioMentoria._internal({required this.id, required mentoria, required usuario, createdAt, updatedAt}): _mentoria = mentoria, _usuario = usuario, _createdAt = createdAt, _updatedAt = updatedAt;
  
  factory UsuarioMentoria({String? id, required Mentoria mentoria, required Usuario usuario}) {
    return UsuarioMentoria._internal(
      id: id == null ? UUID.getUUID() : id,
      mentoria: mentoria,
      usuario: usuario);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UsuarioMentoria &&
      id == other.id &&
      _mentoria == other._mentoria &&
      _usuario == other._usuario;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("UsuarioMentoria {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("mentoria=" + (_mentoria != null ? _mentoria!.toString() : "null") + ", ");
    buffer.write("usuario=" + (_usuario != null ? _usuario!.toString() : "null") + ", ");
    buffer.write("createdAt=" + (_createdAt != null ? _createdAt!.format() : "null") + ", ");
    buffer.write("updatedAt=" + (_updatedAt != null ? _updatedAt!.format() : "null"));
    buffer.write("}");
    
    return buffer.toString();
  }
  
  UsuarioMentoria copyWith({String? id, Mentoria? mentoria, Usuario? usuario}) {
    return UsuarioMentoria._internal(
      id: id ?? this.id,
      mentoria: mentoria ?? this.mentoria,
      usuario: usuario ?? this.usuario);
  }
  
  UsuarioMentoria.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _mentoria = json['mentoria']?['serializedData'] != null
        ? Mentoria.fromJson(new Map<String, dynamic>.from(json['mentoria']['serializedData']))
        : null,
      _usuario = json['usuario']?['serializedData'] != null
        ? Usuario.fromJson(new Map<String, dynamic>.from(json['usuario']['serializedData']))
        : null,
      _createdAt = json['createdAt'] != null ? TemporalDateTime.fromString(json['createdAt']) : null,
      _updatedAt = json['updatedAt'] != null ? TemporalDateTime.fromString(json['updatedAt']) : null;
  
  Map<String, dynamic> toJson() => {
    'id': id, 'mentoria': _mentoria?.toJson(), 'usuario': _usuario?.toJson(), 'createdAt': _createdAt?.format(), 'updatedAt': _updatedAt?.format()
  };
  
  Map<String, Object?> toMap() => {
    'id': id, 'mentoria': _mentoria, 'usuario': _usuario, 'createdAt': _createdAt, 'updatedAt': _updatedAt
  };

  static final QueryField ID = QueryField(fieldName: "id");
  static final QueryField MENTORIA = QueryField(
    fieldName: "mentoria",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (Mentoria).toString()));
  static final QueryField USUARIO = QueryField(
    fieldName: "usuario",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (Usuario).toString()));
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "UsuarioMentoria";
    modelSchemaDefinition.pluralName = "UsuarioMentorias";
    
    modelSchemaDefinition.indexes = [
      ModelIndex(fields: const ["mentoriaID"], name: "byMentoria"),
      ModelIndex(fields: const ["usuarioID"], name: "byUsuario")
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.belongsTo(
      key: UsuarioMentoria.MENTORIA,
      isRequired: true,
      targetName: "mentoriaID",
      ofModelName: (Mentoria).toString()
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.belongsTo(
      key: UsuarioMentoria.USUARIO,
      isRequired: true,
      targetName: "usuarioID",
      ofModelName: (Usuario).toString()
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'createdAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'updatedAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
  });
}

class _UsuarioMentoriaModelType extends ModelType<UsuarioMentoria> {
  const _UsuarioMentoriaModelType();
  
  @override
  UsuarioMentoria fromJson(Map<String, dynamic> jsonData) {
    return UsuarioMentoria.fromJson(jsonData);
  }
}