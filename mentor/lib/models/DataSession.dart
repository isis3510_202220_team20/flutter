/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, annotate_overrides, dead_code, dead_codepublic_member_api_docs, depend_on_referenced_packages, file_names, library_private_types_in_public_api, no_leading_underscores_for_library_prefixes, no_leading_underscores_for_local_identifiers, non_constant_identifier_names, null_check_on_nullable_type_parameter, prefer_adjacent_string_concatenation, prefer_const_constructors, prefer_if_null_operators, prefer_interpolation_to_compose_strings, slash_for_doc_comments, sort_child_properties_last, unnecessary_const, unnecessary_constructor_name, unnecessary_late, unnecessary_new, unnecessary_null_aware_assignments, unnecessary_nullable_for_final_variable_declarations, unnecessary_string_interpolations, use_build_context_synchronously

import 'package:amplify_core/amplify_core.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the DataSession type in your schema. */
@immutable
class DataSession extends Model {
  static const classType = const _DataSessionModelType();
  final String id;
  final TemporalDate? _Date;
  final double? _sessionTime;
  final double? _loginTime;
  final double? _loadingTime;
  final TemporalDateTime? _createdAt;
  final TemporalDateTime? _updatedAt;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  TemporalDate? get Date {
    return _Date;
  }
  
  double? get sessionTime {
    return _sessionTime;
  }
  
  double? get loginTime {
    return _loginTime;
  }
  
  double? get loadingTime {
    return _loadingTime;
  }
  
  TemporalDateTime? get createdAt {
    return _createdAt;
  }
  
  TemporalDateTime? get updatedAt {
    return _updatedAt;
  }
  
  const DataSession._internal({required this.id, Date, sessionTime, loginTime, loadingTime, createdAt, updatedAt}): _Date = Date, _sessionTime = sessionTime, _loginTime = loginTime, _loadingTime = loadingTime, _createdAt = createdAt, _updatedAt = updatedAt;
  
  factory DataSession({String? id, TemporalDate? Date, double? sessionTime, double? loginTime, double? loadingTime}) {
    return DataSession._internal(
      id: id == null ? UUID.getUUID() : id,
      Date: Date,
      sessionTime: sessionTime,
      loginTime: loginTime,
      loadingTime: loadingTime);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataSession &&
      id == other.id &&
      _Date == other._Date &&
      _sessionTime == other._sessionTime &&
      _loginTime == other._loginTime &&
      _loadingTime == other._loadingTime;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("DataSession {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("Date=" + (_Date != null ? _Date!.format() : "null") + ", ");
    buffer.write("sessionTime=" + (_sessionTime != null ? _sessionTime!.toString() : "null") + ", ");
    buffer.write("loginTime=" + (_loginTime != null ? _loginTime!.toString() : "null") + ", ");
    buffer.write("loadingTime=" + (_loadingTime != null ? _loadingTime!.toString() : "null") + ", ");
    buffer.write("createdAt=" + (_createdAt != null ? _createdAt!.format() : "null") + ", ");
    buffer.write("updatedAt=" + (_updatedAt != null ? _updatedAt!.format() : "null"));
    buffer.write("}");
    
    return buffer.toString();
  }
  
  DataSession copyWith({String? id, TemporalDate? Date, double? sessionTime, double? loginTime, double? loadingTime}) {
    return DataSession._internal(
      id: id ?? this.id,
      Date: Date ?? this.Date,
      sessionTime: sessionTime ?? this.sessionTime,
      loginTime: loginTime ?? this.loginTime,
      loadingTime: loadingTime ?? this.loadingTime);
  }
  
  DataSession.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _Date = json['Date'] != null ? TemporalDate.fromString(json['Date']) : null,
      _sessionTime = (json['sessionTime'] as num?)?.toDouble(),
      _loginTime = (json['loginTime'] as num?)?.toDouble(),
      _loadingTime = (json['loadingTime'] as num?)?.toDouble(),
      _createdAt = json['createdAt'] != null ? TemporalDateTime.fromString(json['createdAt']) : null,
      _updatedAt = json['updatedAt'] != null ? TemporalDateTime.fromString(json['updatedAt']) : null;
  
  Map<String, dynamic> toJson() => {
    'id': id, 'Date': _Date?.format(), 'sessionTime': _sessionTime, 'loginTime': _loginTime, 'loadingTime': _loadingTime, 'createdAt': _createdAt?.format(), 'updatedAt': _updatedAt?.format()
  };
  
  Map<String, Object?> toMap() => {
    'id': id, 'Date': _Date, 'sessionTime': _sessionTime, 'loginTime': _loginTime, 'loadingTime': _loadingTime, 'createdAt': _createdAt, 'updatedAt': _updatedAt
  };

  static final QueryField ID = QueryField(fieldName: "id");
  static final QueryField DATE = QueryField(fieldName: "Date");
  static final QueryField SESSIONTIME = QueryField(fieldName: "sessionTime");
  static final QueryField LOGINTIME = QueryField(fieldName: "loginTime");
  static final QueryField LOADINGTIME = QueryField(fieldName: "loadingTime");
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "DataSession";
    modelSchemaDefinition.pluralName = "DataSessions";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PUBLIC,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: DataSession.DATE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.date)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: DataSession.SESSIONTIME,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.double)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: DataSession.LOGINTIME,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.double)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: DataSession.LOADINGTIME,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.double)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'createdAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'updatedAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
  });
}

class _DataSessionModelType extends ModelType<DataSession> {
  const _DataSessionModelType();
  
  @override
  DataSession fromJson(Map<String, dynamic> jsonData) {
    return DataSession.fromJson(jsonData);
  }
}