/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, annotate_overrides, dead_code, dead_codepublic_member_api_docs, depend_on_referenced_packages, file_names, library_private_types_in_public_api, no_leading_underscores_for_library_prefixes, no_leading_underscores_for_local_identifiers, non_constant_identifier_names, null_check_on_nullable_type_parameter, prefer_adjacent_string_concatenation, prefer_const_constructors, prefer_if_null_operators, prefer_interpolation_to_compose_strings, slash_for_doc_comments, sort_child_properties_last, unnecessary_const, unnecessary_constructor_name, unnecessary_late, unnecessary_new, unnecessary_null_aware_assignments, unnecessary_nullable_for_final_variable_declarations, unnecessary_string_interpolations, use_build_context_synchronously

import 'ModelProvider.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the MentoriaCategoria type in your schema. */
@immutable
class MentoriaCategoria extends Model {
  static const classType = const _MentoriaCategoriaModelType();
  final String id;
  final Categoria? _categoria;
  final Mentoria? _mentoria;
  final TemporalDateTime? _createdAt;
  final TemporalDateTime? _updatedAt;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  Categoria get categoria {
    try {
      return _categoria!;
    } catch(e) {
      throw new AmplifyCodeGenModelException(
          AmplifyExceptionMessages.codeGenRequiredFieldForceCastExceptionMessage,
          recoverySuggestion:
            AmplifyExceptionMessages.codeGenRequiredFieldForceCastRecoverySuggestion,
          underlyingException: e.toString()
          );
    }
  }
  
  Mentoria get mentoria {
    try {
      return _mentoria!;
    } catch(e) {
      throw new AmplifyCodeGenModelException(
          AmplifyExceptionMessages.codeGenRequiredFieldForceCastExceptionMessage,
          recoverySuggestion:
            AmplifyExceptionMessages.codeGenRequiredFieldForceCastRecoverySuggestion,
          underlyingException: e.toString()
          );
    }
  }
  
  TemporalDateTime? get createdAt {
    return _createdAt;
  }
  
  TemporalDateTime? get updatedAt {
    return _updatedAt;
  }
  
  const MentoriaCategoria._internal({required this.id, required categoria, required mentoria, createdAt, updatedAt}): _categoria = categoria, _mentoria = mentoria, _createdAt = createdAt, _updatedAt = updatedAt;
  
  factory MentoriaCategoria({String? id, required Categoria categoria, required Mentoria mentoria}) {
    return MentoriaCategoria._internal(
      id: id == null ? UUID.getUUID() : id,
      categoria: categoria,
      mentoria: mentoria);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MentoriaCategoria &&
      id == other.id &&
      _categoria == other._categoria &&
      _mentoria == other._mentoria;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("MentoriaCategoria {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("categoria=" + (_categoria != null ? _categoria!.toString() : "null") + ", ");
    buffer.write("mentoria=" + (_mentoria != null ? _mentoria!.toString() : "null") + ", ");
    buffer.write("createdAt=" + (_createdAt != null ? _createdAt!.format() : "null") + ", ");
    buffer.write("updatedAt=" + (_updatedAt != null ? _updatedAt!.format() : "null"));
    buffer.write("}");
    
    return buffer.toString();
  }
  
  MentoriaCategoria copyWith({String? id, Categoria? categoria, Mentoria? mentoria}) {
    return MentoriaCategoria._internal(
      id: id ?? this.id,
      categoria: categoria ?? this.categoria,
      mentoria: mentoria ?? this.mentoria);
  }
  
  MentoriaCategoria.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _categoria = json['categoria']?['serializedData'] != null
        ? Categoria.fromJson(new Map<String, dynamic>.from(json['categoria']['serializedData']))
        : null,
      _mentoria = json['mentoria']?['serializedData'] != null
        ? Mentoria.fromJson(new Map<String, dynamic>.from(json['mentoria']['serializedData']))
        : null,
      _createdAt = json['createdAt'] != null ? TemporalDateTime.fromString(json['createdAt']) : null,
      _updatedAt = json['updatedAt'] != null ? TemporalDateTime.fromString(json['updatedAt']) : null;
  
  Map<String, dynamic> toJson() => {
    'id': id, 'categoria': _categoria?.toJson(), 'mentoria': _mentoria?.toJson(), 'createdAt': _createdAt?.format(), 'updatedAt': _updatedAt?.format()
  };
  
  Map<String, Object?> toMap() => {
    'id': id, 'categoria': _categoria, 'mentoria': _mentoria, 'createdAt': _createdAt, 'updatedAt': _updatedAt
  };

  static final QueryField ID = QueryField(fieldName: "id");
  static final QueryField CATEGORIA = QueryField(
    fieldName: "categoria",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (Categoria).toString()));
  static final QueryField MENTORIA = QueryField(
    fieldName: "mentoria",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (Mentoria).toString()));
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "MentoriaCategoria";
    modelSchemaDefinition.pluralName = "MentoriaCategorias";
    
    modelSchemaDefinition.indexes = [
      ModelIndex(fields: const ["categoriaID"], name: "byCategoria"),
      ModelIndex(fields: const ["mentoriaID"], name: "byMentoria")
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.belongsTo(
      key: MentoriaCategoria.CATEGORIA,
      isRequired: true,
      targetName: "categoriaID",
      ofModelName: (Categoria).toString()
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.belongsTo(
      key: MentoriaCategoria.MENTORIA,
      isRequired: true,
      targetName: "mentoriaID",
      ofModelName: (Mentoria).toString()
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'createdAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.nonQueryField(
      fieldName: 'updatedAt',
      isRequired: false,
      isReadOnly: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
  });
}

class _MentoriaCategoriaModelType extends ModelType<MentoriaCategoria> {
  const _MentoriaCategoriaModelType();
  
  @override
  MentoriaCategoria fromJson(Map<String, dynamic> jsonData) {
    return MentoriaCategoria.fromJson(jsonData);
  }
}