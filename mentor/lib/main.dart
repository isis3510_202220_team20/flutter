import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mentor/view/login/entry.dart';
import 'package:mentor/view_model/counter_view_model.dart';
import 'package:mentor/view_model/login_view_model.dart';
import 'package:mentor/view_model/profile_view_model.dart';
import 'package:provider/provider.dart';

//Main is the first file that flutter run searches and tries to run.
void main() async {
  //Turn off the landscape mode
  CounterViewModel.startTimer();
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  //Run "My app"
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      //It implements the providers at runtime
      providers: [
        ChangeNotifierProvider(
            create: (_) => LoginViewModel(userSignedIn: false)),
        ChangeNotifierProvider(
            create: (_) => ProfileViewModel(userFetched: false, userUpdated: false)),
        ChangeNotifierProvider(create: (_) => CounterViewModel()),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData.light(),
          home: const MyHomePage(title: "Mentor")),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //The app runs this firstly.
  @override
  Widget build(BuildContext context) {
    return const EntryScreen(); //Screen that verifies connection with Amplify
  }
}
