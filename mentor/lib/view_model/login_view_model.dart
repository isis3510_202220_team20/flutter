import 'package:flutter/cupertino.dart';

class LoginViewModel with ChangeNotifier {
  LoginViewModel({required this.userSignedIn});

  bool userSignedIn;

  changeUserStatus(bool changeState) {
    userSignedIn = changeState;
    notifyListeners();
    // printNewSelection();
  }

  bool getSignedStatus() {
    return userSignedIn;
  }
}
