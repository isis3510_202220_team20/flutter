import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:mentor/models/ModelProvider.dart';

class CounterViewModel with ChangeNotifier {
  CounterViewModel();

  double time = 0;
  static final stopwatch = Stopwatch();
  changeTime(double newTime) {
    time = newTime;
    notifyListeners();
    // printNewSelection();
  }

  static startTimer() {
    stopwatch.start();
  }

  static double stopTimer() {
    double mytime = stopwatch.elapsedMilliseconds / 1;
    stopwatch.stop();
    return mytime;
  }

  addLoadingTime(double newTime) async {
    /* try {
      final todo = DataLoading(loadingTime: newTime);
      final request = ModelMutations.create(todo);
      final response = await Amplify.API.mutate(request: request).response;
      final createdTodo = response.data;
      if (createdTodo == null) {
        safePrint('errors: ${response.errors}');
        return;
      }
      safePrint('Mutation result: ${createdTodo.loadingTime}');
    } on ApiException catch (e) {
      safePrint('Mutation failed: $e');
    } */
  }

  double getSignedStatus() {
    return time;
  }
}
