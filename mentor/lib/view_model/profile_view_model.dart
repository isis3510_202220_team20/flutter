import 'package:flutter/cupertino.dart';
import 'package:mentor/models/Usuario.dart';

class ProfileViewModel with ChangeNotifier {

  ProfileViewModel({required this.userFetched, required this.userUpdated});

  bool userFetched = false;
  bool userUpdated = false;

  setUserLoaded (bool userLoaded) {
    userFetched = userLoaded;
    notifyListeners();
  }

  bool getLoadedStatus() {
    return userFetched;
  }

  setUserUpdated (bool setUser) {
    userUpdated = setUser;
    notifyListeners();
  }

  bool getUpdateStatus() {
    return userUpdated;
  }
}