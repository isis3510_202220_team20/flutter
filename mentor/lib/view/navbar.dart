// ignore_for_file: prefer_const_constructors, non_constant_identifier_names, use_build_context_synchronously

import 'package:amplify_api/model_mutations.dart';
import 'package:amplify_api/model_queries.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mentor/models/ModelProvider.dart';
import 'package:mentor/utils/general_utils.dart';
import 'package:mentor/view/calendar/calendar.dart';
import 'package:mentor/view/classes/classes.dart';
import 'package:mentor/view/home/home.dart';
import 'package:mentor/view/notifications/notifications.dart';
import 'package:mentor/view/profile/profile.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';

//@Author: Carlos Figueredo.
class Navbar extends StatefulWidget {
  const Navbar({Key? key}) : super(key: key);

  @override
  State<Navbar> createState() => _NavbarState();
}

class _NavbarState extends State<Navbar> {
  //Function to show the dialog when is pressed the back button
  int current_index = 0;
  final screens = [
    Home(),
    Calendar(),
    Classes(),
    Notifications(),
    Profile(),
  ];

  Future<void> signOutCurrentUser() async {
    try {
      await Amplify.Auth.signOut();
    } on AuthException catch (e) {
      Utils.flushBarErrorMessage(e.message, context);
    }
  }

  Future<bool?> showMyDialog() => showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
            title: Text('Do you want to leave the app?'),
            actions: [
              TextButton(
                  onPressed: () => Navigator.pop(context, false),
                  child: Text('No')),
              TextButton(
                  onPressed: () {
                    SystemNavigator.pop();
                  },
                  child: Text('Yes'))
            ],
          ));

  /// It opens the option to ask the user if they give access to their location.
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high,
        timeLimit: Duration(seconds: 5));

    return position;
  }

  List<String> dataCurrentUser = [];
  Future<void> fetchCurrentUserAttributes() async {
    try {
      final us = await Amplify.Auth.getCurrentUser();
      final result = await Amplify.Auth.fetchUserAttributes();
      for (final element in result) {
        dataCurrentUser.add(element.value);
      }
      dataCurrentUser.add(us.username);
      dataCurrentUser.add(us.userId);
      saveUser(us.userId, dataCurrentUser[2], us.username, dataCurrentUser[3]);
    } on AuthException catch (e) {
      Utils.flushBarErrorMessage(e.message, context);
    }
  }

  /// Saves the user in the database and creates the model in GraphQL
  Future<void> saveUser(
      String pId, String pNombre, String pUsername, String pEmail) async {
    try {
      Usuario usuario = Usuario(
          id: pId,
          nombre_apellido: pNombre,
          esProfesor: false,
          username: pUsername,
          email: pEmail,
          calificacion: 0,
          biografia: "No bio");
      //It verifies if the user exists in graphQL
      final verifyUser = ModelQueries.get(Usuario.classType, usuario.id);
      final responseVerify =
          await Amplify.API.query(request: verifyUser).response;
      if (responseVerify.data?.id != null) {
      } else {
        final requestUser = ModelMutations.create(usuario);
        final response =
            await Amplify.API.mutate(request: requestUser).response;

        Usuario? createdUsuario = response.data;
        if (createdUsuario == null) {
          Utils.flushBarErrorMessage('errors: ${response.errors}', context);
          return;
        }
      }
    } on ApiException catch (e) {
      Utils.flushBarErrorMessage("Exception on saveUser: $e", context);
    }
  }

  void fetchAndSave() {
    _determinePosition();
    fetchCurrentUserAttributes();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => fetchAndSave());
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final shouldPop = await showMyDialog();
        return shouldPop ?? false;
      },
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: IndexedStack(
            index: current_index,
            children: screens,
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: Color.fromARGB(201, 101, 93, 255),
            currentIndex: current_index,
            selectedItemColor: Colors.white,
            onTap: (index) => setState(() {
              current_index = index;
            }),
            // ignore: prefer_const_literals_to_create_immutables
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: 'Home',
                backgroundColor: Colors.blue,
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.calendar_month),
                label: 'Calendar',
                backgroundColor: Colors.red,
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.school),
                label: 'Classes',
                backgroundColor: Colors.yellow,
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.notifications),
                label: 'Notifications',
                backgroundColor: Colors.green,
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.account_box),
                label: 'Profile',
                backgroundColor: Colors.orange,
              ),
            ],
          )),
    );
  }
}
