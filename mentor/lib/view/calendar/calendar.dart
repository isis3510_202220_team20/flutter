// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class Calendar extends StatefulWidget {
  const Calendar({Key? key}) : super(key: key);

  @override
  State<Calendar> createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  int counter = 0;
  //Function to show the dialog when is pressed the back button
  Future<bool?> showMyDialog() => showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
            title: Text('Do you want to log out?'),
            actions: [
              TextButton(
                  onPressed: () => Navigator.pop(context, false),
                  child: Text('No')),
              TextButton(
                  onPressed: () => Navigator.pop(context, true),
                  child: Text('Yes'))
            ],
          ));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Calendar')),
      body: Center(child: Text("Calendar $counter")),
      floatingActionButton: FloatingActionButton(
        onPressed: () => setState(() {
          counter += 1;
        }),
      ),
    );
  }
}
