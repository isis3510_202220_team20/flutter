import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mentor/models/ModelProvider.dart';
import 'package:mentor/view/classes/classesbyMentor.dart';

//@Author Juan David Bautista Parra.
//This view show more information of a mentor.
class ProfileMentor extends StatefulWidget {
  final Usuario? usuarioo;
  const ProfileMentor({super.key, required this.usuarioo});

  @override
  State<ProfileMentor> createState() => _ProfileMentorState();
}

class _ProfileMentorState extends State<ProfileMentor> {
  final double coverHeight = 280;
  final double profileHeight = 144;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Mentor Profile ' + widget.usuarioo!.nombre_apellido!),
          backgroundColor: Color.fromARGB(201, 101, 93, 255),
        ),
        body: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            buildTop(),
            const SizedBox(height: 22),
            Divider(),
            const SizedBox(height: 60),
            buildContent(),
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "About Me.",
                            style: TextStyle(
                                fontSize: 22, color: Colors.purple.shade500),
                          ),
                          const SizedBox(height: 16),
                          Text(
                            widget.usuarioo!.biografia!,
                            style: TextStyle(fontSize: 17, color: Colors.black),
                          ),
                          const SizedBox(height: 20),
                        ]),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                primary: Color.fromARGB(201, 101, 93, 255),
                                padding: EdgeInsets.all(15.0),
                              ),
                              onPressed: (() => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ClassesbyMentor(
                                            usuario: widget.usuarioo,
                                          )))),
                              child: Text("My classes :) ")),
                        ])
                  ],
                ),
              ),
            )
          ],
        ));
  }

  Widget buildContent() => Column(
        children: [
          const SizedBox(height: 22),
          Text(
            widget.usuarioo!.nombre_apellido!,
            style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.bold,
                color: Colors.purple.shade500),
          ),
          const SizedBox(height: 8),
          Text(
            widget.usuarioo!.username!,
            style: TextStyle(fontSize: 20, color: Colors.black),
          ),
          const SizedBox(height: 16),
          Divider()
        ],
      );

  Widget buildTop() {
    final bottom = profileHeight / 2;
    final top = coverHeight - profileHeight / 2;

    return Stack(
      clipBehavior: Clip.none,
      alignment: Alignment.center,
      children: [
        Container(
          margin: EdgeInsets.only(bottom: bottom),
          child: buildCoverImage(),
        ),
        Positioned(
          top: top,
          child: buildProfileImage(),
        )
      ],
    );
  }

  Widget buildCoverImage() => Container(
        color: Colors.grey,
        child: CachedNetworkImage(
          placeholder: (context, url) => Container(
            child: Center(child: Text("Loading Image...")),
          ),
          imageUrl:
              "https://images.ctfassets.net/hrltx12pl8hq/4ACnMj4WVSOZRZt0jHu9h5/1506f652bcd70f4dc3e88219fefea858/shutterstock_739595833-min.jpg?fit=fill&w=800&h=300",
          width: double.infinity,
          height: coverHeight - 140 / 2,
          fit: BoxFit.cover,
        ),
      );

  Widget buildProfileImage() => CircleAvatar(
      radius: profileHeight / 2,
      backgroundColor: Colors.grey.shade200,
      backgroundImage: CachedNetworkImageProvider(
          "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"));
  //backgroundImage: AssetImage("assets/images/IncognitoProfiles.jpg"));

}
