// ignore_for_file: prefer_const_constructors

import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_api/model_queries.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mentor/models/Usuario.dart';
import 'dart:developer';
import 'dart:convert';

import 'package:provider/provider.dart';

import '../../view_model/profile_view_model.dart';



class ProfileEdit extends StatefulWidget {
  const ProfileEdit({Key? key}) : super(key: key);

  @override
  State<ProfileEdit> createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  bool isObscurePassword = true;
  int counter = 0;

  Usuario? varUsuario;

  final nameController = TextEditingController();//text: varUsuario?.nombre_apellido);
  final bioController = TextEditingController();

  Future<AuthUser> getCurrentUser() async {
    final result = await Amplify.Auth.getCurrentUser();
    print(result);
    _changeLoadingStatus(true);
    return result;
  }
  
  var temporalBio= "hasdhashdhashdhasdhashdashdas";
  
  Future<void> queryListItems(Future<AuthUser> usernameFuture) async {
    try {
      print(await usernameFuture);
      var username = (await usernameFuture).username;
      final queryPredicate = Usuario.USERNAME.eq(username);

      final request = ModelQueries.list<Usuario>(Usuario.classType, where: queryPredicate);
      final response = await Amplify.API.query(request: request).response;
      //print(response);
      varUsuario = response.data?.items.first;
      //print(varUsuario);
      setState(() {});
    } on ApiException catch (e) {
      print('Query failed: $e');
    }
    
  }

  _changeLoadingStatus(bool loaded) {
    Provider.of<ProfileViewModel>(context, listen: false)
      .setUserLoaded(loaded);
  }

  _changeUpdatedStatus(bool updated) {
    Provider.of<ProfileViewModel>(context, listen: false)
      .setUserUpdated(updated);
  }

  _getUpdatingStatus() {
    return Provider.of<ProfileViewModel>(context).userUpdated;
  }

  //var userAuth = Amplify.Auth.getCurrentUser();
  //final userNameDelAuth = Amplify.Auth.getCurrentUser();

  //Future<Usuario?> user = queryItem(userAuth);
  
  // Future<void> updateUsuario() async {
  //   const getUsuario = 'getUsuario';
  //   String graphQLDocument = '''query GetUsuario(\$id: ID!) {
  //     $getUsuario(id: \$id) {
  //     _version
  //     }
  //   }
  //   ''';
  //   final getUsuarioRequest = GraphQLRequest<String>(
  //       document: graphQLDocument,
  //       variables: <String, String>{'id': varUsuario!.id},
  //   );

  //   final response =  await Amplify.API.query(request: getUsuarioRequest).response;

  //   if(response.data != null){
  //     final versionNumber = ((json.decode(response.data??"") as Map).cast<String, dynamic>())[getUsuario]["_version"];
  //     const updateUsuario = 'updateUsuario';
  //     String graphQLDocumentUpdate = '''
  //         mutation updateUser {
  //         $updateUsuario(input: {id:\$id, biografia: \$tempBio, _version: \$versionNumber}) {
  //           id
  //           biografia
  //           _lastChangedAt
  //           updatedAt
  //           _version
  //         }
  //       }
  //     ''';
  //     final updateUsuarioRequest = GraphQLRequest<String>(
  //         document: graphQLDocumentUpdate,
  //         variables: <String, String>{'id': varUsuario!.id, "tempBio": temporalBio, "versionNumber": versionNumber.toString() },
  //     );

  //     final responseUpdate =  await Amplify.API.query(request: updateUsuarioRequest).response;
      
  //     if(responseUpdate.data != null){
  //      final usuarioActualizado = (json.decode(response.data??"") as Map).cast<String, dynamic>();
  //      print("usuarioActualizado: $usuarioActualizado");
  //     }
  //   }

  // }

  Future<void> updateUsuario() async {
  if(nameController.text.trim() == ""){
    nameController.text = varUsuario!.nombre_apellido!;
  }
  if(bioController.text.trim() == ""){
    bioController.text = varUsuario!.biografia!;
  }
  final usuarioWithNewAttributes = varUsuario!.copyWith(nombre_apellido: nameController.text, biografia: bioController.text); // REVISAR
  print (usuarioWithNewAttributes);
  final request = ModelMutations.update(usuarioWithNewAttributes);
  final response = await Amplify.API.mutate(request: request).response;
  print(response.data);
  if(response.data != null){
    varUsuario = response.data;
  }
  
  _changeUpdatedStatus(true);
  setState(() {});
}

  @override
  void initState(){
    super.initState();
    var currentAuthUserFuture =  getCurrentUser();
    queryListItems(currentAuthUserFuture);
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    nameController.dispose();
    bioController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //nameController.text = varUsuario?.nombre_apellido??"Loading...";
    //bioController.text = varUsuario?.biografia??"Loading...";
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Profile'),
        backgroundColor: Color.fromARGB(201, 101, 93, 255),
        /*leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
            ),
            onPressed: () {},
          ),**/
        ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 15, top: 20, right: 15),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
              Center(
                child: Stack(
                  children: [
                    Container(
                      width: 130,
                      height: 130,
                      decoration: BoxDecoration(
                        border: Border.all(width: 4, color: Colors.black),
                        boxShadow: [
                          BoxShadow(
                            spreadRadius: 2,
                            blurRadius: 10,
                            color: Colors.black.withOpacity(0.1)
                          )
                        ],
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: CachedNetworkImageProvider(
                            'https://cdn.pixabay.com/photo/2015/07/11/19/23/book-841171_960_720.jpg',
                          ),
                        )
                      ),
                    ),
                    // Positioned(
                    //   bottom: 0,
                    //   right: 0,
                    //   child: Container(
                    //     height: 40,
                    //     width: 40,
                    //     decoration: BoxDecoration(
                    //       shape: BoxShape.circle,
                    //       border: Border.all(
                    //         width: 4,
                    //         color: Colors.black
                    //       ),
                    //       color: Color.fromARGB(201, 101, 93, 255)
                    //     ),
                    //     child: Icon(
                    //       Icons.camera_alt,
                    //       color: Colors.white,
                    //     ),
                    //   )
                    // )
                  ],
                ),
              ),
              SizedBox(height: 15),
              Align(
                child: Text("Add a photo to give more style to your profile!"),
                alignment: Alignment.center
              ),
              SizedBox(height: 30),
              buildTextField("Full Name", varUsuario?.nombre_apellido??"Loading...", false),
               Text(
                "Make sure to use your real name",
                style: TextStyle(
                          color: Colors.grey[600],
                          fontSize: 12),
              ),
              SizedBox(height: 20),
              //usernameTextField("Email", varUsuario?.email??"Loading...", false),
              buildTextField2("Biography", varUsuario?.biografia??"Write a bio pls", false),
              //usernameTextField("Username", varUsuario?.username??"Loading...", false),
              Text(
                "Describe yourself in a short text :)",
                style: TextStyle(
                          color: Colors.grey[600],
                          fontSize: 12),
              ),
              SizedBox(height: 40),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("CANCEL", style: TextStyle(
                        fontSize: 15,
                        letterSpacing: 2,
                        color: Colors.black
                      )),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.grey[400],
                      padding: EdgeInsets.symmetric(horizontal: 50),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateUsuario();
                    },
                    child: Text("SAVE", style: TextStyle(
                      fontSize: 15,
                      letterSpacing: 2,
                      color: Colors.white
                    )),
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromARGB(201, 101, 93, 255),
                      padding: EdgeInsets.symmetric(horizontal: 50),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildTextField(String labelText, String placeholder, bool isPasswordTextField) {
    return Padding(
      padding: EdgeInsets.only(bottom: 30),
      child: TextFormField(
        controller: nameController,
        obscureText: isPasswordTextField ? isObscurePassword : false,
        decoration: InputDecoration(
          suffixIcon: isPasswordTextField ?
            IconButton(
              icon: Icon(Icons.remove_red_eye, color: Colors.grey),
              onPressed: () {},
            ): null,
          contentPadding: EdgeInsets.only(bottom: 5),
          labelStyle: TextStyle(
            color: Colors.grey[600],
          ),
          labelText: labelText,
          floatingLabelBehavior: FloatingLabelBehavior.always,
          hintText: placeholder,
          hintStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.normal,
            color: Colors.black
          )
        ),
        // validator: (nameController.text) {
        //   if(nameController.text.length < 5){
        //     return "Full Name should be at least 5 characters";
        //   }
        // },
      ),
    );
  }

  Widget buildTextField2(String labelText, String placeholder, bool isPasswordTextField) {
    return Padding(
      padding: EdgeInsets.only(bottom: 30),
      child: TextFormField(
        keyboardType: TextInputType.multiline,
        maxLines: 5,
        controller: bioController,
        //initialValue: varUsuario!.biografia, // Check --------------------------------------------------------------
        obscureText: isPasswordTextField ? isObscurePassword : false,
        decoration: InputDecoration(
          suffixIcon: isPasswordTextField ?
            IconButton(
              icon: Icon(Icons.remove_red_eye, color: Colors.grey),
              onPressed: () {},
            ): null,
          contentPadding: EdgeInsets.only(bottom: 5),
          labelStyle: TextStyle(
            color: Colors.grey[600],
          ),
          labelText: labelText,
          floatingLabelBehavior: FloatingLabelBehavior.always,
          hintText: placeholder,
          hintStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.normal,
            color: Colors.black
          )
        ),
      ),
    );
  }

  Widget usernameTextField(String labelText, String placeholder, bool isPasswordTextField) {
    return Padding(
      padding: EdgeInsets.only(bottom: 30),
      child: TextField(
        readOnly: true,
        obscureText: isPasswordTextField ? isObscurePassword : false,
        decoration: InputDecoration(
          suffixIcon: isPasswordTextField ?
            IconButton(
              icon: Icon(Icons.remove_red_eye, color: Colors.grey),
              onPressed: () {},
            ): null,
          contentPadding: EdgeInsets.only(bottom: 5),
          labelStyle: TextStyle(
            color: Colors.grey[600],
          ),
          labelText: labelText,
          floatingLabelBehavior: FloatingLabelBehavior.always,
          hintText: placeholder,
          hintStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.normal,
            color: Colors.grey
          )
        ),
      ),
    );
  }
}
