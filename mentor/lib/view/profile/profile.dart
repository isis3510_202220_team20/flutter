// ignore_for_file: prefer_const_constructors

import 'dart:isolate';

import 'package:amplify_api/model_queries.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mentor/utils/general_utils.dart';
import 'package:mentor/view/login/login.dart';
import 'package:mentor/models/ModelProvider.dart';
import 'package:mentor/view/profile/profileEdit.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:mentor/view_model/login_view_model.dart';
import 'package:mentor/view_model/profile_view_model.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:cached_network_image/cached_network_image.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool isObscurePassword = true;
  int counter = 0;

  Usuario? varUsuario;

  _removeLoginSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('username');
  }

  Future<AuthUser> getCurrentUser() async {
    final result = await Amplify.Auth.getCurrentUser();
    print(result);
    _changeLoadingStatus(true);
    return result;
  }

  Future<void> queryListItems(Future<AuthUser> usernameFuture) async {
    try {
      print(await usernameFuture);
      var username = (await usernameFuture).username;
      final queryPredicate = Usuario.USERNAME.eq(username);

      final request =
          ModelQueries.list<Usuario>(Usuario.classType, where: queryPredicate);
      final response = await Amplify.API.query(request: request).response;
      varUsuario = response.data?.items.first;
      setState(() {});
    } on ApiException catch (e) {
      print('Query failed: $e');
    }
  }

  refreshView() {
    var currentAuthUserFuture = getCurrentUser();
    queryListItems(currentAuthUserFuture);
  }

  _changeSigninStatus(bool status) {
    Provider.of<LoginViewModel>(context, listen: false)
        .changeUserStatus(status);
  }

  late BuildContext dialogContext = context;
  Future<void> signOutCurrentUser() async {
    try {
      final result = await Amplify.Auth.signOut();
      _changeSigninStatus(false);
      _removeLoginSharedPreferences();
    } on AuthException catch (e) {
      print(e.message);
      Utils.flushBarErrorMessage(e.message, context);
    }
  }

  _changeLoadingStatus(bool loaded) {
    Provider.of<ProfileViewModel>(context, listen: false)
      .setUserLoaded(loaded);
  }

  _getLoadingStatus() {
    return Provider.of<ProfileViewModel>(context).userFetched;
  }

  Future<bool?> showMyDialog() => showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
            title: Text('Do you want to log out?'),
            actions: [
              TextButton(
                  onPressed: () => Navigator.pop(context, false),
                  child: Text('No')),
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    Utils.showMyDialog(
                        context, dialogContext, "Logging out...");
                    signOutCurrentUser();
                    Utils.dismissMyDialog(dialogContext);

                    Navigator.pop(context);
                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => Login()));
                  },
                  child: Text('Yes'))
            ],
          ));

  @override
  void initState() {
    var currentAuthUserFuture = getCurrentUser();
    compute (queryListItems, currentAuthUserFuture);
    setState(() { });
    super.initState();
    queryListItems(currentAuthUserFuture);
  }

  @override
  Widget build(BuildContext context) {
    ProfileViewModel profileViewModel = context.watch<ProfileViewModel>(); // ----------------------------------- CHECK!!!!!!
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
        backgroundColor: Color.fromARGB(201, 101, 93, 255),
        actions: [
          IconButton(
            icon: Icon(
              Icons.edit,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ProfileEdit()),
              ).then((_) => refreshView());
            },
          )
        ],
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 15, top: 20, right: 15),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
              Center(
                child: Stack(
                  children: [
                    Container(
                      width: 130,
                      height: 130,
                      decoration: BoxDecoration(
                        border: Border.all(width: 4, color: Colors.black),
                        boxShadow: [
                          BoxShadow(
                              spreadRadius: 2,
                              blurRadius: 10,
                              color: Colors.black.withOpacity(0.1))
                        ],
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: CachedNetworkImageProvider(
                            'https://cdn.pixabay.com/photo/2015/07/11/19/23/book-841171_960_720.jpg',
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 30),
              buildTextField("Full Name",
                  varUsuario?.nombre_apellido ?? "Loading...", false),
              buildTextField("Email", varUsuario?.email ?? "Loading...", false),
              bioTextField("Biography",
                  varUsuario?.biografia ?? "Write a bio pls", false),
              usernameTextField(
                  "Username", varUsuario?.username ?? "Loading...", false),
              SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      showMyDialog();
                    },
                    child: Text("LOG OUT",
                        style: TextStyle(
                            fontSize: 15,
                            letterSpacing: 2,
                            color: Colors.white)),
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(201, 101, 93, 255),
                        padding: EdgeInsets.symmetric(horizontal: 50),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildTextField(
      String labelText, String placeholder, bool isPasswordTextField) {
    return Padding(
      padding: EdgeInsets.only(bottom: 30),
      child: TextField(
        readOnly: true,
        obscureText: isPasswordTextField ? isObscurePassword : false,
        decoration: InputDecoration(
            suffixIcon: isPasswordTextField
                ? IconButton(
                    icon: Icon(Icons.remove_red_eye, color: Colors.grey),
                    onPressed: () {},
                  )
                : null,
            contentPadding: EdgeInsets.only(bottom: 5),
            labelStyle: TextStyle(
              color: Colors.grey[600],
            ),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.normal,
                color: Colors.black)),
      ),
    );
  }

  Widget bioTextField(
      String labelText, String placeholder, bool isPasswordTextField) {
    return Padding(
      padding: EdgeInsets.only(bottom: 30),
      child: TextField(
        maxLines: 5,
        readOnly: true,
        obscureText: isPasswordTextField ? isObscurePassword : false,
        decoration: InputDecoration(
            suffixIcon: isPasswordTextField
                ? IconButton(
                    icon: Icon(Icons.remove_red_eye, color: Colors.grey),
                    onPressed: () {},
                  )
                : null,
            contentPadding: EdgeInsets.only(bottom: 5),
            labelStyle: TextStyle(
              color: Colors.grey[600],
            ),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.normal,
                color: Colors.black)),
      ),
    );
  }

  Widget usernameTextField(
      String labelText, String placeholder, bool isPasswordTextField) {
    return Padding(
      padding: EdgeInsets.only(bottom: 30),
      child: TextField(
        readOnly: true,
        obscureText: isPasswordTextField ? isObscurePassword : false,
        decoration: InputDecoration(
            suffixIcon: isPasswordTextField
                ? IconButton(
                    icon: Icon(Icons.remove_red_eye, color: Colors.grey),
                    onPressed: () {},
                  )
                : null,
            contentPadding: EdgeInsets.only(bottom: 5),
            labelStyle: TextStyle(
              color: Colors.grey[600],
            ),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.normal,
                color: Colors.black)),
      ),
    );
  }
}
