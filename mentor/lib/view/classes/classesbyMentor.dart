import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:amplify_api/model_queries.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:mentor/models/Mentoria.dart';
import 'package:mentor/models/ModelProvider.dart';
import 'package:mentor/view/classes/classesCard.dart';

class ClassesbyMentor extends StatefulWidget {
  final Usuario? usuario;
  const ClassesbyMentor({super.key, required this.usuario});

  @override
  State<ClassesbyMentor> createState() => _ClassesbyMentorState();
}

Future<List<Mentoria?>> queryListMentorias() async {
  try {
    final request = ModelQueries.list(Mentoria.classType);
    final response = await Amplify.API.query(request: request).response;

    final todos = response.data?.items;
    if (todos == null) {
      print('errors: ${response.errors}');
      return <Mentoria?>[];
    }
    return todos;
  } on ApiException catch (e) {
    print('Query failed: $e');
  }
  return <Mentoria?>[];
}

class _ClassesbyMentorState extends State<ClassesbyMentor> {
  late List<Mentoria?> _mentorias;
  late List<Mentoria?> _limpias = [];
  bool _isLoading = true;

  static List<Mentoria?> newArray(Map map) {
    List<Mentoria?> originalList = map[0];
    List<Mentoria?> listFinal = [];
    String? idUser = map[1];
    for (int i = 0; i < originalList.length; i++) {
      if (originalList[i]!.usuarioID == idUser) {
        listFinal.add(originalList[i]);
      }
    }
    return listFinal;
  }

  Future<void> getMentorias() async {
    _mentorias = await queryListMentorias();
    Map map1 = Map();
    map1[0] = _mentorias;
    map1[1] = widget.usuario!.id;
    _limpias = await compute<Map, List<Mentoria?>>(newArray, map1);
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getMentorias();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title:
              Text('Clases of the mentor ${widget.usuario!.nombre_apellido}'),
          backgroundColor: Color.fromARGB(201, 101, 93, 255),
        ),
        body: _isLoading
            ? const Center(child: CircularProgressIndicator())
            : _limpias.isEmpty
                ? Center(
                    child: Container(
                      width: 300,
                      height: 100,
                      padding: const EdgeInsets.all(20),
                      color: const Color.fromARGB(201, 101, 93, 255),
                      child: Center(
                          child: Text(
                        "No Mentorships Available for this mentor ${widget.usuario!.nombre_apellido!}",
                        style: const TextStyle(fontSize: 25),
                      )),
                    ),
                  )
                : Center(
                    child: ListView.builder(
                        itemCount: _limpias.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          final String tutor =
                              "${widget.usuario!.nombre_apellido}";
                          const String time = "2";
                          final String name = "${_limpias[index]!.nombre}";
                          return ClassesCard(
                              tutor: tutor, time: time, name: name);
                        }),
                  ));
  }
}
