import 'package:amplify_api/model_queries.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:mentor/models/Usuario.dart';
import 'package:mentor/view/profile/profileMentor.dart';
import 'package:mentor/view/subjects/teacherCard.dart';
import 'dart:math';
import 'package:mentor/utils/general_utils.dart';
import 'package:mentor/utils/NetworkConnectivity.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

//@Author Juan David Bautista Parra.
//This view Let a logged User to find a Mentor in the app.
class FindTeacher extends StatefulWidget {
  const FindTeacher({super.key});

  @override
  State<FindTeacher> createState() => _FindTeacherState();
}

Future<List<Usuario?>> queryListMentors() async {
  try {
    final request = ModelQueries.list(Usuario.classType);
    final response = await Amplify.API.query(request: request).response;

    final todos = response.data?.items;
    if (todos == null) {
      print('errors: ${response.errors}');
      return <Usuario?>[];
    }
    return todos;
  } on ApiException catch (e) {
    print('Query failed: $e');
  }
  return <Usuario?>[];
}

Usuario? mentorRandom(List<Usuario?> lista) {
  Random random = Random();
  int randomNumber = random.nextInt(lista.length);
  Usuario? randomMentor;
  if (lista[randomNumber]!.esProfesor == true) {
    randomMentor = lista[randomNumber];
  } else {
    randomMentor = mentorRandom(lista);
  }
  return randomMentor;
}

Usuario? bestScoreMentor(List<Usuario?> lista) {
  double? score = 0;
  Usuario? resp;
  for (int i = 0; i < lista.length; i++) {
    if (lista[i]!.esProfesor == true) {
      if (lista[i]!.calificacion! > score!) {
        score = lista[i]!.calificacion;
        resp = lista[i];
      }
    }
  }
  return resp;
}

class _FindTeacherState extends State<FindTeacher> {
  late List<Usuario?> _usuarios;
  Usuario? mejor;
  Usuario? usuarioRandom;

  bool _isLoading = true;
  @override
  void initState() {
    super.initState();
    getMentors();
  }

  static int fibonacci(int n) {
    if (n < 2) {
      return n;
    }
    return fibonacci(n - 2) + fibonacci(n - 1);
  }

  //Shows connection
  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  String string = '';
  bool notConnected = false;

  Future<void> checkConection() async {
    _networkConnectivity.initialise();
    _networkConnectivity.myStream.listen((source) {
      _source = source;
      print('source $_source');
      // 1.
      switch (_source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          string =
              _source.values.toList()[0] ? 'Mobile: Online' : 'Mobile: Offline';
          notConnected = false;
          break;
        case ConnectivityResult.wifi:
          string =
              _source.values.toList()[0] ? 'WiFi: Online' : 'WiFi: Offline';
          notConnected = false;
          break;
        case ConnectivityResult.none:
        default:
          string = 'Offline';
          notConnected = true;
      }
      // 2.
      setState(() {});
      // 3.
      Utils.flushBarErrorMessage(string, context);
    });
  }

  Future<void> getMentors() async {
    _usuarios = await queryListMentors();

    await checkConection();

    //int hola = await compute<int, int>(fibonacci, 40);
    //print(hola);

    usuarioRandom =
        await compute<List<Usuario?>, Usuario?>(mentorRandom, _usuarios);

    mejor = await compute<List<Usuario?>, Usuario?>(bestScoreMentor, _usuarios);

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Find a Teacher'),
        backgroundColor: Color.fromARGB(201, 101, 93, 255),
      ),
      body: notConnected
          ? Center(
              child: Container(
                width: 300,
                height: 100,
                padding: const EdgeInsets.all(20),
                color: Color.fromARGB(201, 101, 93, 255),
                child: Center(child: Text("No Conection avaliable, Return home page", style: TextStyle(fontSize: 25),)),
              ),
            )
          : (_isLoading
              ? Center(child: CircularProgressIndicator())
              : Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Padding(
                            padding: EdgeInsets.all(5.0),
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  primary: Color.fromARGB(201, 101, 93, 255),
                                  padding: EdgeInsets.all(15.0),
                                ),
                                onPressed: (() => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProfileMentor(
                                              usuarioo: usuarioRandom,
                                            )))),
                                child: Text("Random Mentor")),
                          ),
                          Padding(
                            padding: EdgeInsets.all(5.0),
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  primary: Color.fromARGB(201, 101, 93, 255),
                                  padding: EdgeInsets.all(15.0),
                                ),
                                onPressed: (() => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProfileMentor(
                                              usuarioo: mejor,
                                            )))),
                                child: Text("Best Mentor")),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                          itemCount: _usuarios.length,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            if (_usuarios[index]!.esProfesor == true) {
                              return TeacherCard(
                                  usuarioo: _usuarios[index],
                                  tutor: _usuarios[index]!.nombre_apellido!,
                                  rating: _usuarios[index]!
                                      .calificacion!
                                      .toString(),
                                  press: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ProfileMentor(
                                                usuarioo: _usuarios[index],
                                              ))));
                            } else {
                              return Column();
                            }
                          }),
                    ),
                  ],
                )),
    );
  }
}
