// ignore_for_file: prefer_const_constructors

import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:mentor/models/Mentoria.dart';
import 'package:mentor/utils/NetworkConnectivity.dart';
import 'package:mentor/utils/general_utils.dart';
import 'package:mentor/view/classes/classesCard.dart';

class Classes extends StatefulWidget {
  const Classes({Key? key}) : super(key: key);

  @override
  State<Classes> createState() => _ClassesState();
}

Future<List<Mentoria?>> queryListMentorias() async {
  try {
    final request = ModelQueries.list(Mentoria.classType);
    final response = await Amplify.API.query(request: request).response;

    final todos = response.data?.items;
    if (todos == null) {
      print('errors: ${response.errors}');
      return <Mentoria?>[];
    }
    return todos;
  } on ApiException catch (e) {
    print('Query failed: $e');
  }
  return <Mentoria?>[];
}

class _ClassesState extends State<Classes> {
  late List<Mentoria?> _mentorias;
  late List<Mentoria?> _limpias = [];
  bool _isLoading = true;
//Shows connection
  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  String string = '';
  bool notConnected = false;

  Future<void> checkConection() async {
    _networkConnectivity.initialise();
    _networkConnectivity.myStream.listen((source) {
      _source = source;
      print('source $_source');
      // 1.
      switch (_source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          string =
              _source.values.toList()[0] ? 'Mobile: Online' : 'Mobile: Offline';
          notConnected = false;
          break;
        case ConnectivityResult.wifi:
          string =
              _source.values.toList()[0] ? 'WiFi: Online' : 'WiFi: Offline';
          notConnected = false;
          break;
        case ConnectivityResult.none:
        default:
          string = 'Offline';
          notConnected = true;
      }
      // 2.
      setState(() {});
      // 3.
      Utils.flushBarErrorMessage(string, context);
    });
  }

  Future<void> getMentorias() async {
    _mentorias = await queryListMentorias();
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getMentorias();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('My Classes'),
          backgroundColor: Color.fromARGB(201, 101, 93, 255),
        ),
        body: notConnected
            ? Center(
                child: Container(
                  width: 300,
                  height: 100,
                  padding: const EdgeInsets.all(20),
                  color: Color.fromARGB(201, 101, 93, 255),
                  child: Center(
                      child: Text(
                    "No Conection avaliable, Return home page",
                    style: TextStyle(fontSize: 25),
                  )),
                ),
              )
            : (_isLoading
                ? const Center(child: CircularProgressIndicator())
                : _mentorias.isEmpty
                    ? Center(
                        child: Container(
                          width: 300,
                          height: 100,
                          padding: const EdgeInsets.all(20),
                          color: const Color.fromARGB(201, 101, 93, 255),
                          child: Center(
                              child: Text(
                            "No Mentorships Available",
                            style: const TextStyle(fontSize: 25),
                          )),
                        ),
                      )
                    : Center(
                        child: ListView.builder(
                            itemCount: _mentorias.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              final String tutor =
                                  "${_mentorias[index]!.ciudad}";
                              const String time = "2";
                              final String name =
                                  "${_mentorias[index]!.nombre}";
                              return ClassesCard(
                                  tutor: tutor, time: time, name: name);
                            }),
                      )));
  }
}
