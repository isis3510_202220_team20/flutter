// ignore_for_file: prefer_const_constructors

import 'package:amplify_api/model_queries.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:mentor/models/Mentoria.dart';
import 'package:mentor/view/notifications/notificationsDetail.dart';

class Notifications extends StatefulWidget {
  const Notifications({Key? key}) : super(key: key);

  @override
  State<Notifications> createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  int counter = 0;
  //Function to show the dialog when is pressed the back button
  Future<bool?> showMyDialog() => showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
            title: Text('Do you want to log out?'),
            actions: [
              TextButton(
                  onPressed: () => Navigator.pop(context, false),
                  child: Text('No')),
              TextButton(
                  onPressed: () => Navigator.pop(context, true),
                  child: Text('Yes'))
            ],
          ));
  
  var arrMentorias = <Mentoria?>[];
  
  Future<List<Mentoria?>> queryListMentorias() async {
    try {
      final request = ModelQueries.list(Mentoria.classType, limit: 12);
      final response = await Amplify.API.query(request: request).response;

      final todos = response.data?.items;
      arrMentorias = response.data!.items;
      setState(() {});
      if (todos == null) {
        print('errors: ${response.errors}');
        return <Mentoria?>[];
      }
      return todos;
    } on ApiException catch (e) {
      print('Query failed: $e');
    }
    return arrMentorias;
  }

  refreshView() {
    queryListMentorias();
  }

  @override
  void initState() {
    super.initState();
    queryListMentorias();
    refreshView();
  }

  var messages = const [
    "Welcome to Mentor!",
    "How to make your profile more complete",
    "Change to the schedule of your lesson",
    "Your lesson will start soon"
  ];

  var messages2 = const [
    "This is Mentor, an app created to help you in your most important subjects",
    "Try changing your profile picture so people can identify you easier, also try writing a little description in your profile",
    "Your lesson of tomorrow has been moved to a new date",
    "Don't forget to arrive on time so you can take advantage of the time required"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notifications'),
        backgroundColor: Color.fromARGB(201, 101, 93, 255),
        ),
      body: ListView.separated(
        itemCount: arrMentorias.length,
        separatorBuilder: (context, index) => Divider(),
        itemBuilder: (BuildContext context, int index) {
          var elemento = arrMentorias[index];
          return ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => NotificationsDetail()),
              ).then((_) => refreshView());
            },
            style: ElevatedButton.styleFrom(
              primary: Colors.white
            ),
            child: ListTile(
              title: Text(elemento?.nombre??"Loading..."),
              isThreeLine: true,
              leading: CircleAvatar(
                backgroundColor: Color.fromARGB(201, 101, 93, 255),
                child: Text(
                  style: TextStyle(color: Colors.white),
                  "N"
                  ),
              ),
              subtitle: Text(elemento?.fechaInicio.toString()??"Loading..."),
            )
          );
        },
      )
    );
  }
}
