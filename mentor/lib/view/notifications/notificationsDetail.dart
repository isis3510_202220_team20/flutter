import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_api/model_queries.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mentor/models/Mentoria.dart';
import 'package:mentor/models/Usuario.dart';
import 'dart:developer';
import 'dart:convert';

class NotificationsDetail extends StatefulWidget {
  const NotificationsDetail({Key? key}) : super(key: key);

  @override
  State<NotificationsDetail> createState() => _NotificationsDetailState();
}

class _NotificationsDetailState extends State<NotificationsDetail> {
  
  Mentoria? varMentoria;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notification Info'),
        backgroundColor: Color.fromARGB(201, 101, 93, 255),
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 15, top: 20, right: 15),
        child: ListView(
          children: [
            SizedBox(height: 30),
            // buildTextField("Name",
            //       varMentoria?.nombre ?? "Loading..."),
            // buildTextField("Place",
            //       varMentoria?.lugar ?? "Loading..."),
            // buildTextField("Topic",
            //       varMentoria?.topic ?? "Loading..."),
            buildTextField("Name", "Physics 2"),
            buildTextField("Place", "Zoom"),
            buildTextField("Topic", "Electromagnetism"),
          ],
        ),
      ),
    );
  }

  Widget buildTextField(
      String labelText, String placeholder) {
    return Padding(
      padding: EdgeInsets.only(bottom: 30),
      child: TextField(
        readOnly: true,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 5),
            labelStyle: TextStyle(
              color: Colors.grey[600],
            ),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.normal,
                color: Colors.black)),
      ),
    );
  }

}