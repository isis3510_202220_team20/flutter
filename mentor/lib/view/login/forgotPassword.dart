import 'dart:io';

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:mentor/utils/general_utils.dart';
import 'package:mentor/view/login/confirmResetPassword.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();

  //Check connection if login button is used
  bool activeConnection = false;
  Future checkUserConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() {
          activeConnection = true;
        });

        resetPassword();
      }
    } on SocketException catch (_) {
      setState(() {
        activeConnection = false;
        Utils.flushBarErrorMessage("There is no connection", context);
      });
    }
  }

  // Create this value on the class level to use as a state
  bool isPasswordReset = false;

  Future<void> resetPassword() async {
    try {
      final result = await Amplify.Auth.resetPassword(
        username: _usernameController.text.trim(),
      );
      setState(() {
        isPasswordReset = result.isPasswordReset;
      });

      Future.delayed(const Duration(milliseconds: 200), () {
        Navigator.pop(context);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => ConfirmResetPassword(
                      usernamePassed: _usernameController.text.trim(),
                    )));
      });
    } on AmplifyException catch (e) {
      safePrint(e);
      Utils.flushBarErrorMessage(e.message, context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Form(
          autovalidateMode: AutovalidateMode
              .onUserInteraction, //check for validation while typing
          key: formkey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Center(
                  child: SizedBox(
                      width: 200,
                      height: 150,
                      child: Image.asset("assets/images/mentorlogo.jpeg")),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _usernameController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Username',
                      hintText: 'Enter valid username'),
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: MultiValidator([
                    RequiredValidator(errorText: "* Required"),
                    MinLengthValidator(8,
                        errorText: "Username should be at least 8 characters"),
                  ]),
                  style: const TextStyle(color: Colors.black),
                  maxLength: 20,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                ),
              ),
              Container(
                height: 50,
                width: 250,
                decoration: BoxDecoration(
                    color: const Color.fromARGB(201, 101, 93, 255),
                    borderRadius: BorderRadius.circular(20)),
                child: TextButton(
                  onPressed: () {
                    if (formkey.currentState!.validate()) {
                      checkUserConnection();
                    }
                  },
                  child: const Text(
                    'Send',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
