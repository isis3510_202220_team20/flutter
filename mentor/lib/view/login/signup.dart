// ignore_for_file: sort_child_properties_last, use_build_context_synchronously
import 'dart:io';
import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:mentor/models/Usuario.dart';
import 'package:mentor/utils/NetworkConnectivity.dart';
import 'package:mentor/utils/general_utils.dart';
import 'package:mentor/view/login/confirmSignUp.dart';
import 'package:mentor/view/login/login.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _repeatPasswordController = TextEditingController();
  final _emailController = TextEditingController();
  final _usernameController = TextEditingController();
  final _nameController = TextEditingController();

  //Shows connection
  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  String string = '';
  @override
  void initState() {
    super.initState();
    _networkConnectivity.initialise();
    _networkConnectivity.myStream.listen((source) {
      _source = source;
      // 1.
      switch (_source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          string =
              _source.values.toList()[0] ? 'Mobile: Online' : 'Mobile: Offline';
          break;
        case ConnectivityResult.wifi:
          string =
              _source.values.toList()[0] ? 'WiFi: Online' : 'WiFi: Offline';
          break;
        case ConnectivityResult.none:
        default:
          string = 'Offline';
      }
      // 2.
      setState(() {});
      // 3.
      Utils.flushBarErrorMessage(string, context);
    });
  }

  //Check connection if login button is used
  bool activeConnection = false;
  Future checkUserConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() {
          activeConnection = true;
        });

        signUpUser(
            _usernameController.text.trim(),
            _emailController.text.trim(),
            _nameController.text.trim(),
            _passwordController.text.trim(),
            _repeatPasswordController.text.trim());
      }
    } on SocketException catch (_) {
      setState(() {
        activeConnection = false;
        Utils.flushBarErrorMessage("There is no connection", context);
      });
    }
  }

// Create a boolean for checking the sign up status
  bool isSignUpComplete = false;

  Future<void> signUpUser(String pUsername, String pEmail, String pName,
      String pPassword, String pRepPass) async {
    try {
      final userAttributes = <CognitoUserAttributeKey, String>{
        CognitoUserAttributeKey.name: pName,
        CognitoUserAttributeKey.email: pEmail

        // additional attributes as needed
      };

      //Verify same passwords
      final pass1 = _passwordController.text.trim();
      final pass2 = _repeatPasswordController.text.trim();
      bool samePassword = pass1 == pass2;
      if (!samePassword) {
        Utils.flushBarErrorMessage("The passwords should be the same", context);
      }

      //Verify if the email is registered
      final request = ModelQueries.list(Usuario.classType);
      final response = await Amplify.API.query(request: request).response;
      final usuarios = response.data?.items;
      bool foundExistentEmail = false;
      if (usuarios != null) {
        for (var u in usuarios) {
          if (u!.email == _emailController.text.trim()) {
            Utils.flushBarErrorMessage("The email is registered", context);
            foundExistentEmail = true;
          }
        }
      }
      if (foundExistentEmail == false && samePassword) {
        final result = await Amplify.Auth.signUp(
          username: pUsername,
          password: pPassword,
          options: CognitoSignUpOptions(userAttributes: userAttributes),
        );
        setState(() {
          isSignUpComplete = result.isSignUpComplete;
        });

        Future.delayed(const Duration(milliseconds: 200), () {
          Navigator.pop(context);
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => ConfirmSignUp(
                        usernamePassed: _usernameController.text.trim(),
                      )));
        });
      }
    } on AuthException catch (e) {
      safePrint(e.message);
      Utils.flushBarErrorMessage(e.message, context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Form(
          autovalidateMode: AutovalidateMode
              .onUserInteraction, //check for validation while typing
          key: formkey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Center(
                  child: SizedBox(
                      width: 200,
                      height: 150,
                      child: Image.asset("assets/images/mentorlogo.jpeg")),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                  controller: _usernameController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Username',
                      hintText: 'Enter valid username'),
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: MultiValidator([
                    RequiredValidator(errorText: "* Required"),
                    MinLengthValidator(8,
                        errorText: "Username should be at least 8 characters"),
                  ]),
                  style: const TextStyle(color: Colors.black),
                  maxLength: 20,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                  controller: _emailController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                      hintText: 'Enter valid email'),
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: MultiValidator([
                    RequiredValidator(errorText: "* Required"),
                    MinLengthValidator(8,
                        errorText: "Email should be at least 3 characters"),
                    EmailValidator(errorText: "Enter valid email id")
                  ]),
                  style: const TextStyle(color: Colors.black),
                  maxLength: 50,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                  controller: _nameController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Name',
                      hintText: 'Enter valid name'),
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: MultiValidator([
                    RequiredValidator(errorText: "* Required"),
                    MinLengthValidator(8,
                        errorText: "Name should be at least 8 characters"),
                  ]),
                  style: const TextStyle(color: Colors.black),
                  maxLength: 30,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                      hintText: 'Enter secure password'),
                  validator: MultiValidator([
                    RequiredValidator(errorText: "* Required"),
                    MinLengthValidator(8,
                        errorText: "Password should be at least 8 characters"),
                    MaxLengthValidator(15,
                        errorText:
                            "Password should not be greater than 15 characters"),
                  ]),
                  style: const TextStyle(color: Colors.black),
                  maxLength: 20,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                  //validatePassword,        //Function to check validation
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                  controller: _repeatPasswordController,
                  obscureText: true,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Repeat Password',
                      hintText: 'Enter secure password'),
                  validator: MultiValidator([
                    RequiredValidator(errorText: "* Required"),
                    MinLengthValidator(8,
                        errorText: "Password should be at least 8 characters"),
                    MaxLengthValidator(15,
                        errorText:
                            "Password should not be greater than 15 characters"),
                  ]),
                  style: const TextStyle(color: Colors.black),
                  maxLength: 20,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                  //validatePassword,        //Function to check validation
                ),
              ),
              Container(
                height: 50,
                width: 250,
                decoration: BoxDecoration(
                    color: const Color.fromARGB(201, 101, 93, 255),
                    borderRadius: BorderRadius.circular(20)),
                child: TextButton(
                  onPressed: () {
                    if (formkey.currentState!.validate()) {
                      checkUserConnection();
                    }
                  },
                  child: const Text(
                    'Sign Up',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ),
              const SizedBox(
                height: 60,
              ),
              Row(
                children: <Widget>[
                  const Text('Already have an account?'),
                  TextButton(
                    child: const Text(
                      'Log In',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color.fromARGB(201, 101, 93, 255)),
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (_) => const Login()));
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
