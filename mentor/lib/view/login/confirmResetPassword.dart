import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:mentor/utils/general_utils.dart';
import 'package:mentor/view/login/login.dart';

class ConfirmResetPassword extends StatefulWidget {
  const ConfirmResetPassword({Key? key, required this.usernamePassed})
      : super(key: key);
  final String usernamePassed;
  @override
  State<ConfirmResetPassword> createState() => _ConfirmResetPasswordState();
}

class _ConfirmResetPasswordState extends State<ConfirmResetPassword> {
  final _codeController = TextEditingController();
  final _repeatedPasswordController = TextEditingController();
  final _newPasswordController = TextEditingController();
  late final _username;

  Future<void> confirmResetPassword() async {
    try {
      await Amplify.Auth.confirmResetPassword(
          username: _username,
          newPassword: _newPasswordController.text.trim(),
          confirmationCode: _codeController.text.trim());
    } on AmplifyException catch (e) {
      Utils.flushBarErrorMessage(e.message, context);
    }
  }

  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    _username = widget.usernamePassed;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Form(
          autovalidateMode: AutovalidateMode
              .onUserInteraction, //check for validation while typing
          key: formkey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Center(
                  child: SizedBox(
                      width: 200,
                      height: 150,
                      child: Image.asset("assets/images/mentorlogo.jpeg")),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                  controller: _newPasswordController,
                  obscureText: true,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'New Password',
                      hintText: 'Enter secure password'),
                  validator: MultiValidator([
                    RequiredValidator(errorText: "* Required"),
                    MinLengthValidator(8,
                        errorText: "Password should be at least 8 characters"),
                    MaxLengthValidator(15,
                        errorText:
                            "Password should not be greater than 15 characters"),
                  ]),
                  style: const TextStyle(color: Colors.black),
                  maxLength: 20,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                  //validatePassword,        //Function to check validation
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                  controller: _repeatedPasswordController,
                  obscureText: true,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Repeat Password',
                      hintText: 'Enter secure password'),
                  validator: MultiValidator([
                    RequiredValidator(errorText: "* Required"),
                    MinLengthValidator(8,
                        errorText: "Password should be at least 8 characters"),
                    MaxLengthValidator(15,
                        errorText:
                            "Password should not be greater than 15 characters"),
                  ]),
                  style: const TextStyle(color: Colors.black),
                  maxLength: 20,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                  //validatePassword,        //Function to check validation
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                  controller: _codeController,
                  obscureText: false,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Code',
                      hintText: 'Insert code sent to email'),
                  validator: MultiValidator([
                    RequiredValidator(errorText: "* Required"),
                  ]),
                  style: const TextStyle(color: Colors.black),
                  maxLength: 6,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                  //validatePassword,        //Function to check validation
                ),
              ),
              Container(
                height: 50,
                width: 250,
                decoration: BoxDecoration(
                    color: Colors.purple,
                    borderRadius: BorderRadius.circular(20)),
                child: TextButton(
                  onPressed: () {
                    if (formkey.currentState!.validate()) {
                      confirmResetPassword();
                      Navigator.pop(context);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (_) => const Login()));
                    }
                  },
                  child: const Text(
                    'Send',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class User {
  final String username;
  User({required this.username});
}

final List<User> initialData =
    List.generate(1, (index) => User(username: "Username $index"));

class UserProvider with ChangeNotifier {
  final List<User> _usernames = initialData;
  List<User> get usernames => _usernames;
}
