// ignore_for_file: sort_child_properties_last, use_build_context_synchronously

import 'dart:io';

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:mentor/utils/NetworkConnectivity.dart';
import 'package:mentor/utils/general_utils.dart';
import 'package:mentor/view/login/forgotPassword.dart';
import 'package:mentor/view/login/signup.dart';
import 'package:mentor/view/navbar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _usernameController = TextEditingController();

  //Shows connection
  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  String string = '';
  @override
  void initState() {
    super.initState();
    _networkConnectivity.initialise();
    _networkConnectivity.myStream.listen((source) {
      _source = source;
      // 1.
      switch (_source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          string =
              _source.values.toList()[0] ? 'Mobile: Online' : 'Mobile: Offline';
          break;
        case ConnectivityResult.wifi:
          string =
              _source.values.toList()[0] ? 'WiFi: Online' : 'WiFi: Offline';
          break;
        case ConnectivityResult.none:
        default:
          string = 'Offline';
      }
      // 2.
      setState(() {});
      // 3.
      Utils.flushBarErrorMessage(string, context);
    });
  }

  //Check connection if login button is used
  bool activeConnection = false;
  Future checkUserConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() {
          activeConnection = true;
        });

        signInUser(
            _usernameController.text.trim(), _passwordController.text.trim());
      }
    } on SocketException catch (_) {
      setState(() {
        activeConnection = false;
        Utils.flushBarErrorMessage("There is no connection", context);
      });
    }
  }

  _saveLoginSharedPreferences(String username) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('username', username);
  }

  String? validatePassword(String value) {
    RegExp regex =
        RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');
    if (value.isEmpty) {
      return "* Required";
    } else if (value.length < 6) {
      return "Password should be atleast 6 characters";
    } else if (value.length > 20) {
      return "Password should not be greater than 20 characters";
    } else if (!regex.hasMatch(value)) {
      return 'Enter valid password';
    } else {
      return null;
    }
  }

  late BuildContext dialogContext = context;
// Create a boolean for checking the sign in status
  Future<void> signInUser(String username, String password) async {
    try {
      Utils.showMyDialog(context, dialogContext, "Logging in...");
      await Amplify.Auth.signIn(
        username: username,
        password: password,
      );
      Utils.dismissMyDialog(context);
      // Set signed in status

      _saveLoginSharedPreferences(username);

      Future.delayed(const Duration(milliseconds: 200), () {
        Navigator.push(
            context, MaterialPageRoute(builder: (_) => const Navbar()));
      });
    } on AuthException catch (e) {
      safePrint(e.message);
      Utils.dismissMyDialog(context);
      Utils.flushBarErrorMessage(e.message, context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Form(
          autovalidateMode: AutovalidateMode
              .onUserInteraction, //check for validation while typing
          key: formkey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Center(
                  child: SizedBox(
                      width: 200,
                      height: 150,
                      child: CachedNetworkImage(
                        placeholder: (context, url) =>
                            const Center(child: Text("Loading logo...")),
                        imageUrl: 'https://i.imgur.com/L9ZSGpb.jpg',
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _usernameController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Username',
                      hintText: 'Enter valid username'),
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: MultiValidator([
                    RequiredValidator(errorText: "* Required"),
                    MinLengthValidator(8,
                        errorText: "Username should be at least 8 characters"),
                  ]),
                  style: const TextStyle(color: Colors.black),
                  maxLength: 20,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                      hintText: 'Enter secure password'),
                  validator: MultiValidator([
                    RequiredValidator(errorText: "* Required"),
                    MinLengthValidator(8,
                        errorText: "Password should be at least 8 characters"),
                    MaxLengthValidator(15,
                        errorText:
                            "Password should not be greater than 15 characters"),
                  ]),
                  style: const TextStyle(color: Colors.black),
                  maxLength: 20,
                  maxLengthEnforcement: MaxLengthEnforcement.enforced,
                  //validatePassword,        //Function to check validation
                ),
              ),
              TextButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => const ForgotPassword()));
                },
                child: const Text(
                  'Forgot Password',
                  style: TextStyle(
                      color: Color.fromARGB(201, 101, 93, 255), fontSize: 15),
                ),
              ),
              Container(
                height: 50,
                width: 250,
                decoration: BoxDecoration(
                    color: const Color.fromARGB(201, 101, 93, 255),
                    borderRadius: BorderRadius.circular(20)),
                child: TextButton(
                  onPressed: () {
                    if (formkey.currentState!.validate()) {
                      checkUserConnection();
                      // signInUser(_usernameController.text.trim(),
                      //     _passwordController.text.trim());
                    }
                  },
                  child: const Text(
                    'Login',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ),
              const SizedBox(
                height: 100,
              ),
              Row(
                children: <Widget>[
                  const Text('Does not have account?'),
                  TextButton(
                    child: const Text(
                      'Sign Up',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color.fromARGB(201, 101, 93, 255)),
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (_) => const SignUp()));
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

  /* _changeSigninStatus(bool status) {
    Provider.of<LoginViewModel>(context, listen: false)
        .changeUserStatus(status);
  }

  _addLoadingTime(double time) {
    Provider.of<CounterViewModel>(context, listen: false).addLoadingTime(time);
  } */
 /* bool _userIsLogged() {
    var userIsLogged = _getLoginState();
    String? username = "";
    userIsLogged.then((value) => username = value);

    if (username == null) {
      return false;
    } else {
      return true;
    }
  } */
  /* Future<String?> _getLoginState() async {
    final prefs = await SharedPreferences.getInstance();
    var username = prefs.getString('username');
    return username;
  } */