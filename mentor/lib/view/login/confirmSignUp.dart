// ignore_for_file: use_build_context_synchronously
import 'dart:io';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:mentor/utils/general_utils.dart';
import 'package:mentor/view/navbar.dart';

class ConfirmSignUp extends StatefulWidget {
  const ConfirmSignUp({Key? key, required this.usernamePassed})
      : super(key: key);
  final String usernamePassed;
  @override
  State<ConfirmSignUp> createState() => _ConfirmSignUpState();
}

class _ConfirmSignUpState extends State<ConfirmSignUp> {
  //Check connection if login button is used
  bool activeConnection = false;
  Future checkUserConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() {
          activeConnection = true;
        });

        confirmUser();
      }
    } on SocketException catch (_) {
      setState(() {
        activeConnection = false;
        Utils.flushBarErrorMessage("There is no connection", context);
      });
    }
  }

  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final _codeController = TextEditingController();
  var _username = "";
  Future<void> confirmUser() async {
    try {
      await Amplify.Auth.confirmSignUp(
          username: _username, confirmationCode: _codeController.text.trim());

      Navigator.pop(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => const Navbar()));
    } on AuthException catch (e) {
      safePrint(e.message);
      Utils.flushBarErrorMessage(e.message, context);
    }
  }

  Future<bool?> showMyDialog() => showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
            title: const Text('Do you want to go back?'),
            actions: [
              TextButton(
                  onPressed: () => Navigator.pop(context, false),
                  child: const Text('No')),
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  child: const Text('Yes'))
            ],
          ));

  @override
  Widget build(BuildContext context) {
    _username = widget.usernamePassed;
    return WillPopScope(
      onWillPop: () async {
        final shouldPop = await showMyDialog();
        return shouldPop ?? false;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Form(
            autovalidateMode: AutovalidateMode
                .onUserInteraction, //check for validation while typing
            key: formkey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 60.0),
                  child: Center(
                    child: SizedBox(
                        width: 200,
                        height: 150,
                        child: Image.asset("assets/images/mentorlogo.jpeg")),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 15.0, right: 15.0, top: 15, bottom: 0),
                  child: TextFormField(
                    controller: _codeController,
                    obscureText: false,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Code',
                        hintText: 'Insert code sent to email'),
                    validator: MultiValidator([
                      RequiredValidator(errorText: "* Required"),
                    ]),
                    style: const TextStyle(color: Colors.black),
                    maxLength: 6,
                    maxLengthEnforcement: MaxLengthEnforcement.enforced,
                    //validatePassword,        //Function to check validation
                  ),
                ),
                Container(
                  height: 50,
                  width: 250,
                  decoration: BoxDecoration(
                      color: Colors.purple,
                      borderRadius: BorderRadius.circular(20)),
                  child: TextButton(
                    onPressed: () {
                      if (formkey.currentState!.validate()) {
                        checkUserConnection();
                      }
                    },
                    child: const Text(
                      'Send',
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
