import 'package:amplify_analytics_pinpoint/amplify_analytics_pinpoint.dart';
import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:amplify_datastore/amplify_datastore.dart';
import 'package:flutter/material.dart';
import 'package:mentor/amplifyconfiguration.dart';
import 'package:mentor/models/ModelProvider.dart';
import 'package:mentor/utils/general_utils.dart';
import 'package:mentor/view/login/login.dart';
import 'package:mentor/view/navbar.dart';
import 'package:mentor/view_model/counter_view_model.dart';
import 'package:mentor/view_model/login_view_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

//@Author: Carlos Figueredo
//Entry screen that verifies connection with Amplify
class EntryScreen extends StatefulWidget {
  const EntryScreen({Key? key}) : super(key: key);

  @override
  State<EntryScreen> createState() => _EntryScreenState();
}

class _EntryScreenState extends State<EntryScreen> {
  final amplify = Amplify;
  bool _amplifyConfigured = false;

  @override
  void initState() {
    super.initState();
    _configureAmplify();
  }

  Future<String?> _getLoginState() async {
    final prefs = await SharedPreferences.getInstance();
    var username = prefs.getString('username');

    return username;
  }

  bool userSignedIn = false;
  void setUserSignState(bool state) {
    userSignedIn = state;
  }

  bool _userIsLogged() {
    var userIsLogged = _getLoginState();
    userIsLogged.then((value) {
      if (value != null) {
        setUserSignState(true);
      } else {
        setUserSignState(false);
      }
    });
    return userSignedIn;
  }

  void _configureAmplify() async {
    final auth = AmplifyAuthCognito();
    final analytics = AmplifyAnalyticsPinpoint();
    final api = AmplifyAPI(modelProvider: ModelProvider.instance);
    try {
      if (!Amplify.isConfigured) {
        amplify.addPlugin(api);
        amplify.addPlugin(auth);
        amplify.addPlugin(analytics);
        await Amplify.addPlugin(
            AmplifyDataStore(modelProvider: ModelProvider.instance));
        await amplify.configure(amplifyconfig);
        setState(() {
          _amplifyConfigured = true;
        });
      } else {
        Utils.flushBarErrorMessage("Cannot connect", context);
      }
    } catch (e) {
      Utils.flushBarErrorMessage(e.toString(), context);
    }
  }

  //Add loading time of the app
  _addLoadingTime(double time) {
    Provider.of<CounterViewModel>(context, listen: false).addLoadingTime(time);
  }

  @override
  Widget build(BuildContext context) {
    var time = CounterViewModel.stopTimer();
    _addLoadingTime(time);

    var userLogged = _userIsLogged();
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: const Color.fromARGB(255, 218, 196, 255),
        body: _amplifyConfigured
            ? userLogged
                ? const Navbar()
                : const Login()
            : const Center(child: CircularProgressIndicator()));
  }
}
