// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:mentor/view/subjects/createMentorship.dart';
import 'package:mentor/view/classes/findTeacher.dart';
import 'package:mentor/view/subjects/subjects.dart';

//@Author: Carlos Figueredo.
class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        backgroundColor: Color.fromARGB(201, 101, 93, 255),
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(top: 150),
        child: Column(
          children: [
            Center(
              child: Text(
                "Welcome,",
                style: TextStyle(fontSize: 25, color: Colors.black),
              ),
            ),
            Center(
              child: Text(
                "What do you want for today?",
                style: TextStyle(fontSize: 20, color: Colors.black),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Column(
                children: [
                  Center(
                    child: SizedBox(
                      width: 220.0,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FindTeacher()));
                        },
                        child: Text("Find a teacher"),
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromARGB(201, 101, 93, 255)),
                      ),
                    ),
                  ),
                  Center(
                    child: SizedBox(
                      width: 220.0,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Subjects()));
                        },
                        child: Text("See mentorship marketplace"),
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromARGB(201, 101, 93, 255)),
                      ),
                    ),
                  ),
                  Center(
                    child: SizedBox(
                      width: 220.0,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CreateMentorship()));
                        },
                        child: Text("Create mentorship"),
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromARGB(201, 101, 93, 255)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
