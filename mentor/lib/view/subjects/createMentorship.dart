import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:toggle_switch/toggle_switch.dart';

//@Author Juan David Bautista Parra.
//This view represent a form where a Logged User can create a Mentorship.
class CreateMentorship extends StatefulWidget {
  const CreateMentorship({super.key});

  @override
  State<CreateMentorship> createState() => _CreateMentorshipState();
}

class _CreateMentorshipState extends State<CreateMentorship> {
  final nameController = TextEditingController();
  final descriptionController = TextEditingController();
  final yourNameController = TextEditingController();
  final studentCapacityController = TextEditingController();
  final locationController = TextEditingController();
  final cityController = TextEditingController();

  void onSubmit() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Create Mentorship'),
          backgroundColor: Color.fromARGB(201, 101, 93, 255),
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: Form(
              child: Column(
            children: <Widget>[
              TextField(
                controller: nameController,
                decoration: InputDecoration(
                  labelText: "Name:",
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              TextField(
                  controller: descriptionController,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    fillColor: Color.fromARGB(201, 205, 168, 238),
                    labelText: "Description:",
                    border: OutlineInputBorder(),
                  )),
              SizedBox(
                height: 20.0,
              ),
              TextField(
                  controller: yourNameController,
                  decoration: InputDecoration(
                    labelText: "Your Name:",
                    border: OutlineInputBorder(),
                  )),
              SizedBox(
                height: 20.0,
              ),
              TextField(
                  controller: studentCapacityController,
                  decoration: InputDecoration(
                    labelText: "Student Capacity:",
                    border: OutlineInputBorder(),
                  )),
              SizedBox(
                height: 20.0,
              ),
              TextField(
                  controller: cityController,
                  decoration: InputDecoration(
                    labelText: "City:",
                    border: OutlineInputBorder(),
                  )),
              SizedBox(
                height: 20.0,
              ),
              TextField(
                  controller: locationController,
                  decoration: InputDecoration(
                    labelText: "Location:",
                    border: OutlineInputBorder(),
                  )),
              SizedBox(
                height: 20.0,
              ),
              TextField(
                  controller: locationController,
                  decoration: InputDecoration(
                    labelText: "Location:",
                    border: OutlineInputBorder(),
                  )),
              SizedBox(
                height: 20.0,
              ),
              TextField(
                  controller: locationController,
                  decoration: InputDecoration(
                    labelText: "Price in COP:",
                    border: OutlineInputBorder(),
                  )),
              DateTimePicker(
                type: DateTimePickerType.dateTimeSeparate,
                dateMask: 'd MMM, yyyy',
                initialValue: DateTime.now().toString(),
                firstDate: DateTime.now(),
                lastDate: DateTime(2100),
                icon: Icon(Icons.event),
                dateLabelText: 'Initial Date',
                timeLabelText: "Initial Hour",
                onChanged: (val) => print(val),
                validator: (val) {
                  print(val);
                  return null;
                },
                onSaved: (val) => print(val),
              ),
              DateTimePicker(
                type: DateTimePickerType.dateTimeSeparate,
                dateMask: 'd MMM, yyyy',
                initialValue: DateTime.now().toString(),
                firstDate: DateTime.now(),
                lastDate: DateTime(2100),
                icon: Icon(Icons.event),
                dateLabelText: 'Final  Date',
                timeLabelText: "Final Hour",
                onChanged: (val) => print(val),
                validator: (val) {
                  print(val);
                  return null;
                },
                onSaved: (val) => print(val),
              ),
              SizedBox(
                height: 20.0,
              ),
              SizedBox(
                height: 20.0,
              ),
              ToggleSwitch(
                customWidths: [100.0, 100.0],
                cornerRadius: 20.0,
                initialLabelIndex: 0,
                activeBgColors: [
                  [Colors.cyan],
                  [Colors.redAccent]
                ],
                activeFgColor: Colors.white,
                inactiveBgColor: Colors.grey,
                inactiveFgColor: Colors.white,
                totalSwitches: 2,
                labels: ['Virtual', 'Presencial'],
                onToggle: (index) {
                  bool resp = false;
                  if (index == 0) {
                    resp = true;
                  }
                  print('switched to: $resp');
                },
              ),
              SizedBox(
                height: 20.0,
              ),
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
                ElevatedButton(
                  onPressed: () {},
                  child: Text("Create Mentorship"),
                  style: ElevatedButton.styleFrom(
                      primary: Color.fromARGB(201, 101, 93, 255)),
                ),
              ])
            ],
          )),
        ));
  }
}
