import 'package:flutter/cupertino.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter/material.dart';
import 'package:mentor/view/subjects/subjectsCard.dart';

class Subjects extends StatefulWidget {
  const Subjects({Key? key}) : super(key: key);

  @override
  State<Subjects> createState() => _SubjectsState();
}

class _SubjectsState extends State<Subjects> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Mentorship Marketplace'),
        backgroundColor: Color.fromARGB(201, 101, 93, 255),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20.0,
              ),
              ElevatedButton(
                onPressed: () {
                  /*
                  Navigator.push(
                    context, MaterialPageRoute(
                      builder: (context) => MentorsList()));**/
                },
                child: SubjectsCard(name: "Cálculo Diferencial"),
                style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(201, 101, 93, 255),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  padding: EdgeInsets.all(10),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              ElevatedButton(
                onPressed: () {},
                child: SubjectsCard(name: "Cálculo Integral"),
                style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(201, 101, 93, 255),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  padding: EdgeInsets.all(10),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              ElevatedButton(
                onPressed: () {},
                child: SubjectsCard(name: "Álgebra Lineal"),
                style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(201, 101, 93, 255),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  padding: EdgeInsets.all(10),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              ElevatedButton(
                onPressed: () {},
                child: SubjectsCard(name: "Introducción Programación"),
                style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(201, 101, 93, 255),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  padding: EdgeInsets.all(10),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
